import { useState, useEffect } from 'react';
import Header from '../components/Header';

const Test = () => {
  const [nombre1, setNombre1] = useState('');
  const [nombre2, setNombre2] = useState('Nombre2');

  return (
    <>
      <Header />
      <h1>Soy la pagina test</h1>
      <h2>Mi nombre es: {nombre1}</h2>
      <h2>Mi nombre es: {nombre2}</h2>
      <button onClick={() => setNombre1('Federico')}>Cambiar el Nombre1</button>
      <button onClick={() => setNombre2('Mikael')}>Cambiar el nombre2</button>
    </>
  );
};

export default Test;
