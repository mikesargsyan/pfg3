// Vendors
import { useRouter } from 'next/router';
import { Result, Button } from 'antd';
// Components
import Head from '../components/Head';

// Component
const ErroPage = () => {
  const router = useRouter();

  return (
    <>
      <Head title="404" />
      <Result
        status="404"
        title="404"
        subTitle="Perdon, la pagina que estas buscando no existe"
        extra={<Button type="primary" onClick={() => router.push('/')}>Volver a la Home</Button>}
      />
    </>
  );
};

export default ErroPage;
