// Vendors
import styled from '@emotion/styled';
// Components
import Header from '../components/Header';
import Section from '../components/Section';
import Head from '../components/Head';
import Hero from '../components/Hero';

// Component
const Home = () => (
  <>
    <Head />
    <Header />
    <Hero />
    <Section
      src="/images/covid1.png"
      imageAlt="Una chica con COVID-19"
      imageTitle="Una chica con COVID-19"
    />
  </>
);

export default Home;
