// Vendors
import { useRouter } from 'next/router';
// Context
import { useUser } from '../../context/userContext';
// Components
import AuthForm from '../../components/AuthForm';
import Head from '../../components/Head';
import Header from '../../components/Header';

// Component
const Auth = () => {
  const router = useRouter();
  const { user, loadingUser } = useUser();
  const { id } = router.query;

  const getTitle = () => {
    if (id === 'registration') {
      return 'Registro';
    }

    if (id === 'forgot-account') {
      return 'Recuperar cuenta';
    }

    return '';
  };

  if (!loadingUser && !user) {
    return (
      <>
        <Head title={getTitle()} />
        <Header />
        <AuthForm showRegistration={id === 'registration'} showForgotAccount={id === 'forgot-account'} />
      </>
    );
  }

  return null;
};

export default Auth;
