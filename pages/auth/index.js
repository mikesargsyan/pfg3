// Context
import { useUser } from '../../context/userContext';
// Components
import AuthForm from '../../components/AuthForm';
import Head from '../../components/Head';
import Header from '../../components/Header';

// Component
const Auth = () => {
  const { user, loadingUser } = useUser();

  if (!loadingUser && !user) {
    return (
      <>
        <Head />
        <Header />
        <AuthForm />
      </>
    );
  }

  return null;
};

export default Auth;
