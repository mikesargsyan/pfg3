const sgMail = require('@sendgrid/mail');

export default async (req, res) => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  const { user, event } = req.body;

  try {
    await sgMail.send({
      to: event.email,
      from: 'mikesargsyann@gmail.com',
      subject: 'Alguien se dio de baja de tu evento!!',
      html: (
        `
        <h3>A continuacion dejamos los detalles del evento:</h3>
        <p>Categoria: ${event.category}</p>
        <p>Link: ${event.link}</p>
        <p>Plataforma: ${event.platform}</p>
        <p>Fecha: ${event.time}</p>
        <p>Descripcion: ${event.description ? event.description : '-'}</p>
        <br />
        <h3>Detalles del usuario que se inscribio:</h3>
        <p>Nombre: ${user.displayName}</p>
        <p>Email: ${user.email}</p>
        <br />
  
        <p>Saludos,
        <br />
        El equipo de Qarentena
        </p>
        `
      ),
    });

    await sgMail.send({
      to: user.email,
      from: 'mikesargsyann@gmail.com',
      subject: 'Cancelaste una asistencia para un evento!!',
      html: (
        `
        <h3>A continuacion dejamos los detalles del evento que cancelaste:</h3>
        <p>Autor: ${event.autor}</p>
        <p>Email del autor: ${event.email}</p>
        <p>Categoria: ${event.category}</p>
        <p>Link: ${event.link}</p>
        <p>Plataforma: ${event.platform}</p>
        <p>Fecha: ${event.time}</p>
        <p>Descripcion: ${event.description ? event.description : '-'}</p>
        <br />
  
        <p>Saludos,
        <br />
        El equipo de Qarentena
        </p>
        `
      ),
    });

    res.send(200);
  } catch (error) {
    res.send(400);
  }
};
