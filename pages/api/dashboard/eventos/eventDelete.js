const sgMail = require('@sendgrid/mail');

export default async (req, res) => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  const { event } = req.body;

  try {
    for (let i = 0; i < Object.entries(event.inscribedUser).length; i++) {
      // eslint-disable-next-line no-await-in-loop
      if (Object.entries(event.inscribedUser)[i][1].active) {
        await sgMail.send({
          to: Object.entries(event.inscribedUser)[i][1].email,
          from: 'mikesargsyann@gmail.com',
          subject: 'Se cancelo un evento que ibas a asistir :(',
          html: (
            `
            <h3>El autor decidio eliminar el evento, a continuacion dejamos los detalles del evento que cancelo:</h3>
            <p>Autor: ${event.autor}</p>
            <p>Email del autor: ${event.email}</p>
            <p>Categoria: ${event.category}</p>
            <p>Link: ${event.link}</p>
            <p>Plataforma: ${event.platform}</p>
            <p>Fecha: ${event.time}</p>
            <p>Descripcion: ${event.description ? event.description : '-'}</p>
            <br />
      
            <p>Saludos,
            <br />
            El equipo de Qarentena
            </p>
            `
          ),
        });
      }
    }

    res.send(200);
  } catch (error) {
    res.send(400);
  }
};
