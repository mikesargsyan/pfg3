const sgMail = require('@sendgrid/mail');

export default async (req, res) => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  const { user, item } = req.body;

  const requesterInfo = {
    to: item.info.autor,
    from: 'mikesargsyann@gmail.com',
    subject: 'Alguien respondio tu Solicitud!',
    html: (
      `
      <h3>A continuacion dejamos los detalles de la persona asi se comunican:</h3>
      <p>Nombre: ${user.displayName}</p>
      <p>Email: ${user.email}</p>
      <br />


      <p>Saludos,
      <br />
      El equipo de Qarentena
      </p>
      `
    ),
  };

  const answererInfo = {
    to: user.email,
    from: 'mikesargsyann@gmail.com',
    subject: 'Respondiste a la Solicitud de alguien!',
    html: (
      `
      <h3>A continuacion dejamos los detalles de la persona asi se comunican:</h3>
      <p>Nombre: ${item.info.displayName}</p>
      <p>Email: ${item.info.autor}</p>
      <p>Solicitud: ${item.info.title}</p>
      <p>Descripcion: ${item.info.content ? item.info.content : '-'}</p>
      <br />

      <p>Saludos,
      <br />
      El equipo de Qarentena
      </p>
      `
    ),
  };

  try {
    await sgMail.send(requesterInfo);
    await sgMail.send(answererInfo);

    res.send(200);
  } catch (error) {
    res.send(400);
  }
};
