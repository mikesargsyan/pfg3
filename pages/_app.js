// Context
import { UserProvider, UserContext } from '../context/userContext';
import { GeneralProvider, GeneralContext } from '../context/generalContext';
// Componets
import GlobalSpinner from '../components/GlobalSpinner';
import Head from '../components/Head';
// Global styles
import '../assets/styles/global.css';
// Antd styles
import 'antd/dist/antd.css';

// eslint-disable-next-line react/prop-types
const App = ({ Component, pageProps, router }) => (
  <GeneralProvider>
    <UserProvider>
      <GeneralContext.Consumer>
        {(generalContext) => (
          <UserContext.Consumer>
            {(userContext) => (
              <>
                <Head />
                <Component {...pageProps} key={router.route} />
                <GlobalSpinner />
              </>
            )}
          </UserContext.Consumer>
        )}
      </GeneralContext.Consumer>
    </UserProvider>
  </GeneralProvider>
);

export default App;
