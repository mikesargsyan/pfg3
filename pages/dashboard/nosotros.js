// Components
import Head from '../../components/Head';
import Page from '../../components/Page';
import Layout from '../../components/Layout';
// Context
import { useUser } from '../../context/userContext';

// Component
const Nosotros = () => {
  const { user } = useUser();

  return (
    <Page>
      <Head title="Nosotros" />

      <Layout>Nosotros</Layout>
    </Page>
  );
};

export default Nosotros;
