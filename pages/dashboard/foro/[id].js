// Vendors
import React, { useEffect, useState } from 'react';
import {
  Button,
  Comment,
  Form,
  Avatar,
  Input,
  Typography,
  Card,
  Tooltip,
  List,
  notification,
  Skeleton,
} from 'antd';
import {
  ArrowLeftOutlined,
  LikeOutlined,
  LikeFilled
} from '@ant-design/icons';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import moment from 'moment';
// Components
import Head from '../../../components/Head';
import Page from '../../../components/Page';
import Layout from '../../../components/Layout';
// Firebase
import { database } from '../../../firebase/clientApp';
// Context
import { useUser } from '../../../context/userContext';

// Styles
const ButtonLike = styled.button`
  margin-right: 4px;
  cursor: pointer;
  background: transparent;
  border: none;
  box-shadow: none;
`;

// Component
const Article = () => {
  const router = useRouter();
  const { user } = useUser();
  const [loading, setLoading] = useState(true);
  const [commentValue, setCommentValue] = useState('');
  const [submittingComment, setSubmittingComment] = useState(false);
  const [data, setData] = useState({});
  const [queryId, setQueryId] = useState(null);

  useEffect(() => {
    if (router && router.query) {
      setQueryId(router.query.id);
    }
  }, [router]);

  useEffect(() => {
    if (queryId) {
      const articleRef = database.ref('foro/').child(queryId);

      articleRef.on('value', (snapshot) => {
        setLoading(true);

        if (snapshot.val()) {
          setData(snapshot.val());
          setLoading(false);
        }
      });
    }
  }, [queryId]);

  const onChangeComment = (e) => setCommentValue(e.target.value);

  const onSubmitComment = () => {
    if (!commentValue) {
      return;
    }

    setSubmittingComment(true);

    const commentRef = database.ref('foro/').child(queryId).child('comments');
    const newComment = commentRef.push();
    const newCommentKey = newComment.key;

    const createComment = commentRef.push({
      id: newCommentKey,
      userId: user.uid,
      avatar: user.photoURL,
      content: commentValue,
      author: user.displayName,
      date_created: new Date().toString(),
      closed: false,
      likes: [{}],
    });

    createComment
      .then(() => (
        notification.success({
          message: 'Comentario agregado!',
        })
      ))
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ))
      .finally(() => {
        setSubmittingComment(false);
        setCommentValue('');
      });
  };

  const removeCommentary = (id) => {
    const commentRef = database.ref('foro/').child(queryId).child('comments').child(id);

    commentRef.update({
      closed: true,
    }, () => {
      notification.success({
        message: 'Comentario eliminado correctamente!',
      });
    });
  };

  const onLike = (commentId) => {
    const commentRef = database.ref('foro/').child(queryId).child('comments').child(`${commentId}/likes`);

    commentRef.once('value')
      .then((snapshot) => {
        if (snapshot.val()) {
          if (Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)) {
            commentRef.child(Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)[1].key).update({
              amount: Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)[1].actionLiked === 'liked' ? Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)[1].amount - 1 : Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)[1].amount + 1,
              actionLiked: Object.entries(snapshot.val()).find((like) => like[1].id === user.uid)[1].actionLiked === 'liked' ? 'disliked' : 'liked',
            });
          } else {
            const newLike = commentRef.push();
            const newLikeKey = newLike.key;

            newLike.set({
              id: user.uid,
              key: newLikeKey,
              amount: 1,
              actionLiked: 'liked',
            });
          }
        } else {
          const newLike = commentRef.push();
          const newLikeKey = newLike.key;

          newLike.set({
            id: user.uid,
            key: newLikeKey,
            amount: 1,
            actionLiked: 'liked',
          });
        }
      })
      .catch(() => {});
  };

  const commentInput = () => (
    <div>
      <Form.Item>
        <Input.TextArea rows={4} onChange={onChangeComment} value={commentValue} />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" loading={submittingComment} onClick={onSubmitComment} type="primary">
          Agregar comentario
        </Button>
      </Form.Item>
    </div>
  );

  const getLikedIcons = (comment) => {
    if (comment.likes) {
      if (Object.entries(comment.likes).find((like) => like[1].id === user.uid)) {
        if (Object.entries(comment.likes).find((like) => like[1].id === user.uid)[1].actionLiked === 'liked') {
          return <LikeFilled />;
        }
      }

      return <LikeOutlined />;
    }

    return <LikeOutlined />;
  };

  const getLikedTitle = (comment) => {
    if (comment.likes) {
      if (Object.entries(comment.likes).find((like) => like[1].id === user.uid)) {
        if (Object.entries(comment.likes).find((like) => like[1].id === user.uid)[1].actionLiked === 'liked') {
          return 'Sacar like';
        }
      }

      return 'Dar like';
    }

    return 'Dar like';
  };

  const getLikedAmount = (comment) => {
    let amount = 0;

    if (comment.likes) {
      Object.entries(comment.likes).forEach((like) => { amount += like[1].amount; });
    }

    return amount;
  };

  return (
    <Page>
      <Head title="Foro" />

      <Layout>
        {loading ? (
          <Skeleton active />
        ) : (
          <>
            <Button type="primary" onClick={() => router.back()} style={{ marginBottom: 16 }}>
              <ArrowLeftOutlined />
              {' '}
              Ir atras
            </Button>
            <Card>
              <Typography.Title level={2}>{data.title}</Typography.Title>
              <Typography.Title level={4}>{data.description}</Typography.Title>
            </Card>
            {data.comments && (
              <List
                className="comment-list"
                dataSource={Object.entries(data.comments).filter((item) => !item[1].closed)}
                header={`${data.comments ? `${Object.keys(data.comments).map((x) => data.comments[x].closed).filter((bool) => !bool).length} ${Object.keys(data.comments).map((x) => data.comments[x].closed).filter((bool) => !bool).length > 1 ? 'comentarios' : 'comentario'}` : '0 comentarios'}`}
                itemLayout="horizontal"
                renderItem={(item) => (
                  <li>
                    <Comment
                      actions={(
                        [
                          <span key="comment-basic-like">
                            <Tooltip title={getLikedTitle(item[1])}>
                              <ButtonLike onClick={() => onLike(item[0])}>
                                {getLikedIcons(item[1])}
                              </ButtonLike>
                            </Tooltip>
                            <span className="comment-action">{getLikedAmount(item[1])}</span>
                          </span>,
                          user.uid === item[1].userId && <a role="button" key="delete" onClick={() => removeCommentary(item[0])}>eliminar comentario</a>,
                        ]
                      )}
                      author={item[1].author}
                      avatar={item[1].avatar ? item[1].avatar : <Avatar>{item[1].author.split(' ').map((a) => a[0]).join('').toUpperCase()}</Avatar>}
                      content={item[1].content}
                      datetime={moment(item[1].date_created).format('MMMM Do YYYY, h:mm:ss')}
                    />
                  </li>
                )}
              />
            )}
            <Comment
              avatar={user.photoURL}
              content={commentInput()}
            />
          </>
        )}
      </Layout>
    </Page>
  );
};

export default Article;
