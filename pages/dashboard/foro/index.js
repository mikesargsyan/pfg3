// Components
import Head from '../../../components/Head';
import Page from '../../../components/Page';
import Layout from '../../../components/Layout';
import CreatedArticles from '../../../components/CreatedArticles';

// Component
const Foro = () => {

  return (
    <Page>
      <Head title="Foro" />

      <Layout>
        <CreatedArticles />
      </Layout>
    </Page>
  );
};

export default Foro;
