// Vendors
import { useEffect, useState } from 'react';
import {
  Tabs,
  message,
  Select,
  Modal,
  Form,
} from 'antd';
import styled from '@emotion/styled';
// Components
import Head from '../../../components/Head';
import Page from '../../../components/Page';
import Layout from '../../../components/Layout';
import CreateAssistance from '../../../components/CreateAssistance';
import CreatedAssistanceRequest from '../../../components/CreatedAssistanceRequest';
import SearchAssistance from '../../../components/SearchAssistance';

const { TabPane } = Tabs;

const StyledTabs = styled(Tabs)`
  height: 100%;
  overflow: unset;

  .ant-tabs-content {
    height: 100%;
  }
`;

// Component
const Asistencia = () => {
  const [loading, setLoading] = useState(true);
  const [showModalState, setShowModalState] = useState(false);
  const [state, setState] = useState(null);
  const [userPosition, setUserPosition] = useState(null);
  const [loadingMessage] = useState(() => message.loading('Obteniendo tu ubicacion...', 0));
  const [formState] = Form.useForm();

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(() => {}, () => {});

    const permisionsListener = (permissionStatus) => {
      permissionStatus.onchange = () => {
        if (permissionStatus.state === 'granted') {
          navigator.geolocation.getCurrentPosition((position) => setUserPosition(position));
        } else {
          setUserPosition(null);
        }

        setLoading(false);
        setTimeout(loadingMessage, 0);
      };
    };

    if (navigator.permissions) {
      navigator.permissions.query({ name: 'geolocation' })
        .then((permissionStatus) => {
          if (permissionStatus.state === 'granted') {
            navigator.geolocation.getCurrentPosition((position) => setUserPosition(position));
          } else {
            setUserPosition(null);
          }

          setLoading(false);
          setTimeout(loadingMessage, 0);
          permisionsListener(permissionStatus);
        })
        .catch(() => {
          setLoading(false);
          setTimeout(loadingMessage, 0);
        });
    }

    return () => permisionsListener({});
  }, []);

  useEffect(() => {
    if (state) {
      setShowModalState(false);
      localStorage.setItem('state', state);
    }
  }, [state]);

  useEffect(() => {
    setShowModalState(!localStorage.getItem('state'));
    setState(localStorage.getItem('state'));
  }, []);

  return (
    <Page>
      <Head title="Asistencia" />
      <Modal
        key="state-modal"
        title="Selecciona en la provincia que te encontras"
        visible={showModalState}
        footer={null}
        closable={false}
      >
        <Form
          form={formState}
          name="form-state"
          validateMessages={{ required: 'Este campo es requerido' }}
        >
          <Form.Item
            label="Provincia"
            name="state"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Select placeholder="Secciona una opcion" onChange={(e) => setState(e)}>
              <Select.Option value="Buenos Aires">Buenos Aires</Select.Option>
              <Select.Option value="Catamarca">Catamarca</Select.Option>
              <Select.Option value="Chaco">Chaco</Select.Option>
              <Select.Option value="Chubut">Chubut</Select.Option>
              <Select.Option value="Córdoba">Córdoba</Select.Option>
              <Select.Option value="Corrientes">Corrientes</Select.Option>
              <Select.Option value="Entre Ríos">Entre Ríos</Select.Option>
              <Select.Option value="Formosa">Formosa</Select.Option>
              <Select.Option value="Jujuy">Jujuy</Select.Option>
              <Select.Option value="La Pampa">La Pampa</Select.Option>
              <Select.Option value="La Rioja">La Rioja</Select.Option>
              <Select.Option value="Mendoza">Mendoza</Select.Option>
              <Select.Option value="Misiones">Misiones</Select.Option>
              <Select.Option value="Neuquén">Neuquén</Select.Option>
              <Select.Option value="Río Negro">Río Negro</Select.Option>
              <Select.Option value="Salta">Salta</Select.Option>
              <Select.Option value="San Juan">San Juan</Select.Option>
              <Select.Option value="San Luis">San Luis</Select.Option>
              <Select.Option value="Santa Cruz">Santa Cruz</Select.Option>
              <Select.Option value="Santa Fe">Santa Fe</Select.Option>
              <Select.Option value="Santiago del Estero">Santiago del Estero</Select.Option>
              <Select.Option value="Tierra del Fuego, Antártida e Isla del Atlántico Sur">Tierra del Fuego, Antártida e Isla del Atlántico Sur</Select.Option>
              <Select.Option value="Tucumán">Tucumán</Select.Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
      <Layout>
        <StyledTabs defaultActiveKey="1" type="card" size="small">
          <TabPane tab="Buscar solicitudes de asistencia" key="1">
            <SearchAssistance
              loading={loading}
              userPosition={userPosition}
              state={state}
            />
          </TabPane>
          <TabPane tab="Solicitar asistencia" key="2">
            <CreateAssistance
              loading={loading}
              userPosition={userPosition}
            />
          </TabPane>
          <TabPane tab="Mis solicitudes de asistencia" key="3">
            <CreatedAssistanceRequest />
          </TabPane>
        </StyledTabs>
      </Layout>
    </Page>
  );
};

export default Asistencia;
