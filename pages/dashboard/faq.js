// Components
import Head from '../../components/Head';
import Page from '../../components/Page';
import Layout from '../../components/Layout';
// Context
import { useUser } from '../../context/userContext';

// Component
const FAQ = () => {
  const { user } = useUser();

  return (
    <Page>
      <Head title="FAQ" />

      <Layout>FAQ</Layout>
    </Page>
  );
};

export default FAQ;
