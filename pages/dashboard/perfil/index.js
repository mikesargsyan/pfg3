// Components
import Head from '../../../components/Head';
import Page from '../../../components/Page';
import Layout from '../../../components/Layout';
// Context
import { useUser } from '../../../context/userContext';

// Component
const Perfil = () => {
  const { user } = useUser();

  return (
    <Page>
      <Head title="Perfil" />

      <Layout>
        {user ? (
          <>
            <h2>
              Nombre:
              {' '}
              {user.displayName}
            </h2>
            <h2>
              Email:
              {' '}
              {user.email}
            </h2>
          </>
        ) : null}
      </Layout>
    </Page>
  );
};

export default Perfil;
