// Components
import Head from '../../../components/Head';
import Page from '../../../components/Page';
import Layout from '../../../components/Layout';
import CreatedEvents from '../../../components/CreatedEvents';

// Component
const Eventos = () => {

  return (
    <Page>
      <Head title="Eventos" />

      <Layout>
        <CreatedEvents />
      </Layout>
    </Page>
  );
};

export default Eventos;
