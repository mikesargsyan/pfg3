// Components
import Head from '../../components/Head';
import Page from '../../components/Page';
import Layout from '../../components/Layout';
// Context
import { useUser } from '../../context/userContext';

// Component
const Home = () => {
  const { user } = useUser();

  return (
    <Page>
      <Head title="Dashboard" />

      <Layout>Home</Layout>
    </Page>
  );
};

export default Home;
