// Vendors
import styled from '@emotion/styled';
import { Typography } from 'antd';

// Styles
const Wrapper = styled.div`
  padding: 100px 0;
  background:
    url(/images/shape-1.png) no-repeat 103% 0,
    url(../../images/shape-2.png) no-repeat 0% 100%;
  background-size: 120px;
`;

const Container = styled.div`
  max-width: 720px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 10px;
  align-items: center;
  justify-content: center;
  padding: 0 15px;

  @media only screen and (max-width: 767px) {
    grid-template-columns: 1fr;
    grid-template-rows: 0.5fr 1fr;
    padding: 0 30px;
  }
`;

const ImageWrapper = styled.div`
  max-width: 250px;
  width: 100%;
  margin: 0 auto;
  
  @media only screen and (max-width: 767px) {
    margin-bottom: 15px;
  }
`;

const Image = styled.img`
  max-width: 100%;
`;

// Component
const Section = ({ src, imageAlt, imageTitle }) => {
  return (
    <Wrapper>
      <Container>
        <ImageWrapper>
          <Image src={src} alt={imageAlt} title={imageTitle} />
        </ImageWrapper>
        <div>
          <Typography.Title level={3}>Sobre el Coronavirus (COVID-19)</Typography.Title>
          <Typography.Text><strong>La enfermedad por coronavirus (COVID-19)</strong> es una enfermedad infecciosa causada por un coronavirus recientemente descubierto. La mayoría de las personas infectadas con el virus COVID-19 experimentarán una enfermedad respiratoria leve a moderada y se recuperarán sin requerir un tratamiento especial.</Typography.Text>
          <Typography.Text>Existen varias medidas que puede tomar para protegerse del COVID-19</Typography.Text>
          <ul>
            <li>
              <Typography.Text>
                - Lávese bien las manos durante 20 segundos,
              </Typography.Text>
            </li>
            <li>
              <Typography.Text>
                - No te toques la cara,
              </Typography.Text>
            </li>
            <li>
              <Typography.Text>
                - Usa máscara,
              </Typography.Text>
            </li>
            <li>
              <Typography.Text>
                - Evitar reuniones sociales,
              </Typography.Text>
            </li>
            <li>
              <Typography.Text>
                - Frotar tu mano con un desinfectante a base de alcohol,
              </Typography.Text>
            </li>
            <li>
              <Typography.Text>
                - Evite estrechar la mano para protegerse y proteger a otros,
              </Typography.Text>
            </li>
          </ul>
        </div>
      </Container>
    </Wrapper>
  );
};

export default Section;