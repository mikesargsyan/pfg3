// Vendors
import { useEffect } from 'react';
import Link from 'next/link';
import PropTyes from 'prop-types';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import {
  Form,
  Input,
  Button,
  Typography,
  Divider,
} from 'antd';
import {
  FacebookOutlined,
  GoogleOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';
// Context
import { useUser } from '../../context/userContext';

// Styles
const Wrapper = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  > div {
    max-width: 300px;
    width: 100%;
    margin-bottom: 50px;
  }

  form {
    display: flex;
    flex-direction: column;
  }

  .firebaseui-card-content {
    padding: 0;
  }
`;

const StyledForm = styled(Form)`
  max-width: 300px;
  width: 100%;

  button {
    width: 100%;
  }

  p {
    margin: 0;
  }

  .forgot-account {
    text-align: center;
    margin-bottom: 24px;
  }

  .footer-link {
    display: flex;
    flex-direction: row;
    margin-top: 8px;

    a {
      margin-left: 4px;
    }
  }
`;

const SocialMediaWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  margin-top: 24px;
`;

// Component
const AuthForm = ({ showRegistration, showForgotAccount }) => {
  const {
    onSignIn,
    onSignInGoogle,
    onSignInFacebook,
    onSignUp,
    onForgotAccount,
  } = useUser();
  const router = useRouter();

  const validateMessages = {
    required: 'Este campo es requerido',
    types: {
      email: 'No es un email valido',
    },
    string: {
      min: 'Tiene que tener un minimo de ${min} caracteres',
    },
  };

  useEffect(() => {
    if (Object.entries(router.query).length > 0) {
      if (Object.entries(router.query)[0][0] === 'redirection') {
        localStorage.setItem('redirection', JSON.stringify(router.query.redirection));
      }
    }
  }, []);

  if (showRegistration && !showForgotAccount) {
    return (
      <Wrapper>
        <Typography.Title level={3}>Registro</Typography.Title>
        <Divider />
        <StyledForm
          name="signUp"
          onFinish={onSignUp}
          validateMessages={validateMessages}
        >
          <Form.Item
            name="name"
            rules={[{
              required: true,
            }]}
          >
            <Input placeholder="Nombre" />
          </Form.Item>

          <Form.Item
            name="surname"
            rules={[{
              required: true,
            }]}
          >
            <Input placeholder="Apellido" />
          </Form.Item>

          <Form.Item
            name="email"
            rules={[{
              required: true,
              type: 'email',
            }]}
          >
            <Input placeholder="Email" autoComplete="username" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{
              required: true,
              min: 6,
            }]}
          >
            <Input.Password placeholder="Contraseña" autoComplete="current-password" />
          </Form.Item>

          <Button type="primary" htmlType="submit">
            Registrarme
          </Button>

          <div className="footer-link">
            <p>O</p>
            <Link href="/auth">
              <a>iniciar sesion ahora!</a>
            </Link>
          </div>
        </StyledForm>
      </Wrapper>
    );
  }

  if (!showRegistration && !showForgotAccount) {
    return (
      <Wrapper>
        <Typography.Title level={3}>Inicio de sesion</Typography.Title>
        <Divider />
        <StyledForm
          name="signIn"
          onFinish={onSignIn}
          validateMessages={validateMessages}
        >
          <Form.Item
            name="email"
            rules={[{
              required: true,
              type: 'email',
            }]}
          >
            <Input placeholder="Email" autoComplete="username" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{
              required: true,
              min: 6,
            }]}
          >
            <Input.Password placeholder="Contraseña" autoComplete="current-password" />
          </Form.Item>

          <div className="forgot-account">
            <Link href="/auth/[id]" as="/auth/forgot-account">
              <a>Olvide mi cuenta!</a>
            </Link>
          </div>

          <Button type="primary" htmlType="submit">
            Iniciar sesion
          </Button>

          <div className="footer-link">
            <p>O</p>
            <Link href="/auth/[id]" as="/auth/registration">
              <a>registrate ahora!</a>
            </Link>
          </div>
        </StyledForm>

        <SocialMediaWrapper>
          <Button type="primary" onClick={onSignInGoogle}>
            <GoogleOutlined />
          </Button>

          <Button type="primary" onClick={onSignInFacebook}>
            <FacebookOutlined />
          </Button>
        </SocialMediaWrapper>
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <Typography.Title level={3}>Recuperacion de cuenta</Typography.Title>
      <Divider />
      <StyledForm
        name="forgotAccount"
        onFinish={onForgotAccount}
        validateMessages={validateMessages}
      >
        <Form.Item
          name="email"
          rules={[{
            required: true,
            type: 'email',
          }]}
        >
          <Input placeholder="Email" autoComplete="username" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Recuperar mi cuenta
          </Button>
        </Form.Item>

        <Link href="/auth">
          <a><ArrowLeftOutlined /></a>
        </Link>
      </StyledForm>
    </Wrapper>
  );
};

// PropTyes
AuthForm.propTypes = {
  showRegistration: PropTyes.bool,
  showForgotAccount: PropTyes.bool,
};

// DefaultProps
AuthForm.defaultProps = {
  showRegistration: false,
  showForgotAccount: false,
};

export default AuthForm;
