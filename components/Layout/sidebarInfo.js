// Vendors
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import {
  Layout, Menu, List, Carousel,
} from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  UserOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
// Context
import { useUser } from '../../context/userContext';
import { database } from '../../firebase/clientApp';

const { SubMenu } = Menu;
const { Sider } = Layout;

const Logo = styled.div`
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
`;

const StyledMenu = styled(Menu)`
  height: 100%;

  > li:last-child {

  }
`;

// Component
const SidebarInfo = () => {
  const [tips, setTips] = useState([]);
  const [infos, setInfos] = useState([]);

  // Esta es la ruta de la base de datos para obtener los tips
  const tipsReference = database.ref('tips/');
  // Esta es la ruta de la base de datos para obtener los info
  const infosReference = database.ref('info/');

  const [dotPosition] = useState(['right']);

  // Con este metodo llamamos a los tips
  const getTips = () => {
    tipsReference.once('value')
      .then((snapshot) => setTips(Object.entries(snapshot.val()).map((item) => item[1])));
  };

  // Con este metodo llamamos a la info
  const getInfos = () => {
    infosReference.once('value')
      .then((snapshot) => setInfos(Object.entries(snapshot.val()).map((item) => item[1])));
  };

  useEffect(() => {
    // Llamamos al metodo getTips() 1 sola vez cuando el componente se monta
    getTips();
    // Llamamos al metodo getInfos() 1 sola vez cuando el componente se monta
    getInfos();
  }, []);


  const [collapsed, setCollapsed] = useState(false);
  const { onSignOut } = useUser();
  const router = useRouter();

  const navigate = (path, as) => {
    router.push(path, as);
  };

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)}>
      <Logo />

      <Carousel dotPosition={dotPosition} autoplay>
        {tips.map((item) => (<div key={item}><h3 style={{ color: 'white' }}>{item.title}</h3></div>))}
      </Carousel>

      <br />

      <Carousel dotPosition={dotPosition} autoplay>
        {infos.map((item) => (<div key={item}><h3 style={{ color: 'white' }}>{item.title}</h3></div>))}
      </Carousel>


      {/* <List
          header={<h1 style={{ color: 'white' }}>Tips:</h1>}
          bordered
          dataSource={tips}
          renderItem={(item) => (
            <List.Item>
              <h5 style={{ color: 'white' }}>{item.title}</h5>
              <h5 style={{ color: 'white' }}>{item.description}</h5>
            </List.Item>
          )}
        />

        <List
          header={<h1 style={{ color: 'white' }}>Infos:</h1>}
          bordered
          dataSource={infos}
          renderItem={(item) => (
            <List.Item>
              <h5 style={{ color: 'white' }}>{item.title}</h5>
              <h5 style={{ color: 'white' }}>{item.description}</h5>
            </List.Item>
          )}
          /> */}

    </Sider>
  );
};

export default SidebarInfo;
