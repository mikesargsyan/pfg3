// Vendors
import { useState } from 'react';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import { Layout, Menu } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  UserOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
// Context
import { useUser } from '../../context/userContext';

const { SubMenu } = Menu;
const { Sider } = Layout;

const Logo = styled.div`
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
`;

const StyledMenu = styled(Menu)`
  height: 100%;

  > li:last-child {

  }
`;

// Component
const Sidebar = () => {
  const [collapsed, setCollapsed] = useState(false);
  const { onSignOut } = useUser();
  const router = useRouter();

  const navigate = (path, as) => {
    router.push(path, as);
  };

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)}>
      <Logo />
      <Menu theme="dark" defaultSelectedKeys={[router.pathname]} mode="inline">
        <Menu.Item key="1">
          <PieChartOutlined />
          <span>Option 1</span>
        </Menu.Item>
        <Menu.Item key="2">
          <DesktopOutlined />
          <span>Option 2</span>
        </Menu.Item>
        <SubMenu
          key="/dashboard/perfil"
          title={(
            <span>
              <UserOutlined />
              <span>Perfil</span>
            </span>
          )}
          onClick={() => navigate('/dashboard/perfil', 'perfil')}
        >
          <Menu.Item key="3">Configuracion</Menu.Item>
        </SubMenu>
        <Menu.Item onClick={onSignOut}>
          <LogoutOutlined />
          <span>Salir</span>
        </Menu.Item>
      </Menu>
    </Sider>
  );
};

export default Sidebar;
