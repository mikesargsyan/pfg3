// Vendors
import Router from 'next/router';
import styled from '@emotion/styled';
import { Breadcrumb } from 'antd';
import {
  HomeOutlined,
  UserOutlined,
} from '@ant-design/icons';

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;

  > span {
    &:hover {
      color: #40a9ff;
      cursor: pointer;
    }
  }
`;

// Componet
const BreadcrumbComponet = () => {
  return (
    <StyledBreadcrumb>
      <Breadcrumb.Item onClick={() => Router.push('/')}>
        <HomeOutlined />
      </Breadcrumb.Item>
      <Breadcrumb.Item onClick={() => Router.push('/faq')}>
        <UserOutlined />
        <span>Application List</span>
      </Breadcrumb.Item>
      <Breadcrumb.Item>Application</Breadcrumb.Item>
    </StyledBreadcrumb>
  );
};

export default BreadcrumbComponet;
