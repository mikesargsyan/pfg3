// Vendors
import styled from '@emotion/styled';
import { Layout } from 'antd';
// components
import Header from '../Header';
import Footer from '../Footer';
import Sidebar from './sidebar';
import SidebarInfo from './sidebarInfo';
import Breadcrumb from './breadcrumb';
// Context
import { useUser } from '../../context/userContext';

// const { Content, Sider } = Layout;

// Styles
const Wrapper = styled.div`
  background-color: #f0f2f5;
  padding-bottom: 24px;
  height: calc(100% - 64px);
  box-sizing: content-box;
`;

const Content = styled.div`
  background-color: white;
  margin: 24px 24px 0;
  padding: 24px;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #f0f2f5;
  height: 100%;
`;

// Componet
const ComponentLayout = ({ children }) => {
  return (
    <Wrapper>
      <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
      <ContentWrapper>
        <Content>
          {children}
        </Content>
        <Footer />
      </ContentWrapper>
    </Wrapper>
  );
};

export default ComponentLayout;
