// Vendors
import { useState, useEffect } from 'react';
import {
  Form,
  Input,
  Button,
  List,
  Tooltip,
  Modal,
  Select,
  Typography,
  notification,
  Skeleton,
  Divider,
  Spin,
  DatePicker,
} from 'antd';
import { debounce } from 'lodash';
import styled from '@emotion/styled';
import Highlighter from 'react-highlight-words';
import {
  UserOutlined,
} from '@ant-design/icons';
import moment from 'moment';
// Firebase
import { database } from '../../firebase/clientApp';
// Context
import { useUser } from '../../context/userContext';
import { useGeneralContext } from '../../context/generalContext';

// Styles
const StyledList = styled(List)`
`;

const Wrapper = styled.div`
  overflow-y: auto;
  height: 100%;
`;

const Search = styled(Input)`
  max-width: 300px;
`;

// Component
const CreatedEvents = () => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState([]);
  const [loadingList, setLoadingList] = useState(true);
  const [searchedValue, setSearchedValue] = useState('');
  const [filteredData, setFilteredData] = useState([]);
  const [searching, setSearching] = useState(false);
  const [form] = Form.useForm();
  const { user } = useUser();
  const { setGlobalLoading, setGlobalLoadingTip } = useGeneralContext();

  const getInitialData = () => {
    const foroRef = database.ref('eventos/');

    foroRef.on('value', (snapshot) => {
      setLoadingList(true);

      if (snapshot.val()) {
        const items = Object.entries(snapshot.val()).map((item) => ({
          ...item[1],
          id: item[0],
        })).filter((item) => !item.closed);

        setData(items);

        if (loadingList) {
          setLoadingList(false);
        }
      } else {
        setLoadingList(false);
      }
    });
  };

  useEffect(() => {
    getInitialData();
  }, []);

  useEffect(() => {
    let aux = [];

    if (searchedValue) {
      aux = data.filter((item) => (
        item.category.toLowerCase().includes(searchedValue)
        || item.time.toLowerCase().includes(searchedValue)
        || item.link.toLowerCase().includes(searchedValue)
        || item.platform.toLowerCase().includes(searchedValue)
        || item.description.toLowerCase().includes(searchedValue)));
    }

    setFilteredData(aux);
    setSearching(false);
  }, [searchedValue]);

  const onSubmit = ({
    category,
    time,
    platform,
    link,
    description = undefined,
  }) => {
    const eventoRef = database.ref('eventos/');
    const newEvent = eventoRef.push();
    const newEventKey = newEvent.key;
    let createEvent = null;

    if (description) {
      createEvent = newEvent.set({
        id: newEventKey,
        userId: user.uid,
        category,
        time: moment(time).format('DD/MM/YYYY HH:mm'),
        platform,
        link,
        description,
        date_created: new Date().toString(),
        autor: user.displayName,
        email: user.email,
        closed: false,
        inscribedUser: [{}],
      });
    } else {
      createEvent = newEvent.set({
        id: newEventKey,
        userId: user.uid,
        category,
        time: moment(time).format('DD/MM/YYYY HH:mm'),
        platform,
        link,
        date_created: new Date().toString(),
        autor: user.displayName,
        email: user.email,
        closed: false,
        inscribedUser: [{}],
      });
    }

    createEvent
      .then(() => (
        notification.success({
          message: 'Evento creado!',
        })
      ))
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ))
      .finally(() => setShowModal(false));
  };

  const handleModalOk = () => {
    form
      .validateFields()
      .then((values) => {
        onSubmit(values);
        form.resetFields();
      })
      .catch((error) => {})
  };

  const onAssist = (eventId) => {
    const eventRef = database.ref('eventos/').child(eventId).child('inscribedUser');

    eventRef.once('value')
      .then((snapshot) => {
        if (snapshot.val()) {
          if (Object.entries(snapshot.val()).find((suscribe) => suscribe[1].id === user.uid)) {
            database.ref('eventos/').child(eventId).once('value')
              .then((snapshot2) => {
                setGlobalLoading(true);
                setGlobalLoadingTip('Cargando datos...');

                if (snapshot2.val()) {
                  fetch(!Object.entries(snapshot.val()).find((suscribe) => suscribe[1].id === user.uid)[1].active ? (
                    '/api/dashboard/eventos/eventInscription'
                  ) : '/api/dashboard/eventos/eventCancelation', {
                    method: 'post',
                    headers: {
                      Accept: 'application/json, text/plain, */*',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      user,
                      event: snapshot2.val(),
                    }),
                  })
                    .then((res) => {
                      if (res.status !== 200) {
                        throw new Error();
                      } else {
                        eventRef.child(Object.entries(snapshot.val()).find((suscribe) => suscribe[1].id === user.uid)[1].key).update({
                          active: !Object.entries(snapshot.val()).find((suscribe) => suscribe[1].id === user.uid)[1].active,
                        });
                      }
                    })
                    .catch(() => (
                      notification.error({
                        message: 'Ups',
                        description:
                          'Parece que surgio un problema. Por favor, intentalo mas tarde!',
                      })
                    ));
                }
              })
              .finally(() => setGlobalLoading(false));
          } else {
            database.ref('eventos/').child(eventId).once('value')
              .then((snapshot2) => {
                setGlobalLoading(true);
                setGlobalLoadingTip('Cargando datos...');

                if (snapshot2.val()) {
                  fetch('/api/dashboard/eventos/eventInscription', {
                    method: 'post',
                    headers: {
                      Accept: 'application/json, text/plain, */*',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      user,
                      event: snapshot2.val(),
                    }),
                  })
                    .then((res) => {
                      if (res.status !== 200) {
                        throw new Error();
                      } else {
                        const newLike = eventRef.push();
                        const newLikeKey = newLike.key;

                        newLike.set({
                          id: user.uid,
                          key: newLikeKey,
                          active: true,
                          email: user.email,
                        });
                      }
                    });
                }
              })
              .catch(() => (
                notification.error({
                  message: 'Ups',
                  description:
                    'Parece que surgio un problema. Por favor, intentalo mas tarde!',
                })
              ))
              .finally(() => setGlobalLoading(false));
          }
        } else {
          database.ref('eventos/').child(eventId).once('value')
            .then((snapshot2) => {
              setGlobalLoading(true);
              setGlobalLoadingTip('Cargando datos...');

              if (snapshot2.val()) {
                fetch('/api/dashboard/eventos/eventInscription', {
                  method: 'post',
                  headers: {
                    Accept: 'application/json, text/plain, */*',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    user,
                    event: snapshot2.val(),
                  }),
                })
                  .then((res) => {
                    if (res.status !== 200) {
                      throw new Error();
                    } else {
                      const newInscribed = eventRef.push();
                      const newInscribedKey = newInscribed.key;

                      newInscribed.set({
                        id: user.uid,
                        key: newInscribedKey,
                        active: true,
                        email: user.email,
                      });
                    }
                  })
                  .finally(() => setGlobalLoading(false));
              }
            })
            .catch(() => (
              notification.error({
                message: 'Ups',
                description:
                  'Parece que surgio un problema. Por favor, intentalo mas tarde!',
              })
            ));
        }
      });
  };

  const getInscribedUsers = (event) => {
    let amount = 0;

    if (event.inscribedUser) {
      Object.entries(event.inscribedUser).forEach((inscription) => {
        amount += inscription[1].active ? 1 : 0;
      });
    }

    return amount;
  };

  const getAssistanceButtons = (event) => {
    if (event.inscribedUser) {
      if (Object.entries(event.inscribedUser).find((inscription) => inscription[1].id === user.uid)) {
        if (Object.entries(event.inscribedUser).find((inscription) => inscription[1].id === user.uid)[1].active) {
          return <a role="button" key="delete" onClick={() => onAssist(event.id)}>dejar de asistir al evento</a>;
        }
      }

      return <a role="button" key="delete" onClick={() => onAssist(event.id)}>asistir evento</a>;
    }

    return <a role="button" key="delete" onClick={() => onAssist(event.id)}>asistir evento</a>;
  };

  const removeEvent = (id) => {
    const eventRef = database.ref(`eventos/${id}`);

    eventRef.once('value')
      .then((snapshot) => {
        setGlobalLoading(true);
        setGlobalLoadingTip('Eliminando evento...');

        if (snapshot.val()) {
          fetch('/api/dashboard/eventos/eventDelete', {
            method: 'post',
            headers: {
              Accept: 'application/json, text/plain, */*',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              user,
              event: snapshot.val(),
            }),
          })
            .then((res) => {
              if (res.status !== 200) {
                throw new Error();
              } else {
                eventRef.update({
                  closed: true,
                }, () => {
                  notification.success({
                    message: 'Evento eliminado correctamente!',
                  });
                });
              }
            })
            .finally(() => setGlobalLoading(false));
        }
      })
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ));
  };

  const onSearch = (e) => {
    const value = e.target.value.toLowerCase();

    setSearching(true);

    (debounce(() => setSearchedValue(value), 500, { trailing: true, maxWait: 500 }))();
  };

  if (loadingList) {
    return (
      <Skeleton active />
    );
  }

  return (
    <Wrapper>
      <Modal
        title="Crear un evento"
        visible={showModal}
        onOk={handleModalOk}
        okText="Aceptar"
        cancelText="Cancelar"
        onCancel={() => setShowModal(false)}
      >
        <Form
          form={form}
          name="create-event"
          validateMessages={{
            pattern: {
              mismatch: 'El formato de link no es correcto',
            },
            required: 'Este campo es requerido',
          }}
        >
          <Form.Item
            label="Categoria"
            name="category"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Select placeholder="Secciona una opcion">
              <Select.Option value="Curso">Curso</Select.Option>
              <Select.Option value="Entrenamiento">Entrenamiento</Select.Option>
              <Select.Option value="Reunion">Reunion</Select.Option>
              <Select.Option value="Transmision en vivo">Transmision en vivo</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Horario"
            name="time"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <DatePicker
              placeholder="Selecciona una fecha"
              showTime
              style={{ width: '100%' }}
            />
          </Form.Item>

          <Form.Item
            label="Plataforma"
            name="platform"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Input placeholder="Plataforma" />
          </Form.Item>

          <Form.Item
            label="Link del evento"
            name="link"
            rules={[{
              required: true,
              pattern: /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm,
            }]}
            hasFeedback
          >
            <Input type="url" placeholder="Link" />
          </Form.Item>

          <Form.Item
            label="Descripcion"
            name="description"
            rules={[{
              required: false,
            }]}
            hasFeedback
          >
            <Input.TextArea placeholder="Descripcion" />
          </Form.Item>
        </Form>
      </Modal>
      <Search placeholder="Buscar eventos" onChange={onSearch} />
      <Divider />
      <Spin tip="Buscando eventos..." spinning={loadingList}>
        <StyledList
          itemLayout="vertical"
          size="large"
          pagination={{
            pageSize: 10,
          }}
          loading={searching}
          dataSource={searchedValue ? filteredData : data}
          footer={(
            <div>
              <Button onClick={() => setShowModal(true)} type="primary" size="large">Crear un evento</Button>
            </div>
          )}
          renderItem={(item) => (
            <List.Item
              key={item.id}
              actions={[
                <Tooltip title="Horario de comienzo">
                  <Highlighter
                    searchWords={[searchedValue]}
                    autoEscape
                    textToHighlight={item.time}
                  />
                  {' '}
                  hrs
                </Tooltip>,
                <Tooltip title="Autor">
                  {item.autor}
                </Tooltip>,
                <Tooltip title="Cantidad de usuarios inscriptos">
                  <UserOutlined />
                  {' '}
                  {getInscribedUsers(item)}
                </Tooltip>,
                item.userId !== user.uid && getAssistanceButtons(item),
                user.uid === item.userId && <a role="button" key="delete" onClick={() => removeEvent(item.id)}>eliminar evento</a>,
              ].filter(Boolean)}
            >
              <List.Item.Meta
                title={(
                  <Typography.Paragraph>
                    Categoria:
                    {' '}
                    <Highlighter
                      searchWords={[searchedValue]}
                      autoEscape
                      textToHighlight={item.category}
                    />
                  </Typography.Paragraph>
                )}
                description={(
                  item.description && (
                    <Typography.Paragraph>
                      Descripcion:
                      {' '}
                      <Highlighter
                        searchWords={[searchedValue]}
                        autoEscape
                        textToHighlight={item.description}
                      />
                    </Typography.Paragraph>
                  )
                )}
              />
              <Typography.Paragraph>
                <Typography.Paragraph strong>
                  Plataforma a utilizar:
                </Typography.Paragraph>
                <Highlighter
                  searchWords={[searchedValue]}
                  autoEscape
                  textToHighlight={item.platform}
                />
              </Typography.Paragraph>
              <Typography.Paragraph>
                <Typography.Paragraph strong>
                  Link del evento:
                </Typography.Paragraph>
                <Highlighter
                  searchWords={[searchedValue]}
                  autoEscape
                  textToHighlight={item.link}
                />
              </Typography.Paragraph>
            </List.Item>
          )}
        />
      </Spin>
    </Wrapper>
  );
};

export default CreatedEvents;
