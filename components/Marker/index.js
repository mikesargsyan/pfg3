// Vendors
import styled from '@emotion/styled';
import { Tooltip, Typography, Button, Divider } from 'antd';
import { CloseCircleFilled } from '@ant-design/icons';

// Styles
const StyledMarker = styled.div`
  border: 1px solid white;
  border-radius: 50%;
  height: ${(props) => (props.highlight ? 40 : 30)}px;
  width: ${(props) => (props.highlight ? 40 : 30)}px;
  background-color: ${(props) => (props.showInfo ? 'green' : props.highlight ? '#6f1414' : 'gray')};
  cursor: pointer;
  z-index: ${(props) => (props.user ? 20 : 1)};
  transition: 0.2s all linear;

  ${(props) => props.user && `
    background-color: blue;
    width: 15px;
    height: 15px;
    border: 2px solid white;
  `};
`;

const StyledPopover = styled.div`
  font-size: 14px;
  background: white;
  width: 250px;
  padding: 12px 26px 16px 16px;
  border-radius: 8px;
  box-shadow: 0 3px 6px -4px rgba(0,0,0,.12), 0 6px 16px 0 rgba(0,0,0,.08), 0 9px 28px 8px rgba(0,0,0,.05);
  position: relative;
  transition: 0.4s all linear;
  transform: translateY(-275px) translateX(-112px);
  z-index: 2;
  
  .ant-divider {
    margin: 5px 0 12px;
  }

  .anticon-close-circle {
    position: absolute;
    top: 10px;
    cursor: pointer;
    right: 6px;
    font-size: 16px;

    &:hover {
      color: #1890ff; 
    }
  }

  button {
    margin-top: 12px;
    display: block;
  }
`;

const Arrow = styled.div`
  display: block;
  width: 8.48528137px;
  height: 8.48528137px;
  background: 0 0;
  border-style: solid;
  border-width: 4.24264069px;
  bottom: 6.2px;
  border-top-color: transparent;
  border-right-color: #fff;
  border-bottom-color: #fff;
  border-left-color: transparent;
  box-shadow: 3px 3px 7px rgba(0,0,0,.07);
  transform: translateY(10px) rotate(45deg);
  left: 0;
  right: 0;
  margin: auto;
  position: absolute;
`;

// Componets
const Popover = ({
  highlight,
  info,
  onAnswer,
  onClose
}) => (
  <StyledPopover highlight={highlight}>
    <CloseCircleFilled onClick={onClose} />
    <Typography.Text>
      <Typography.Text strong>Titulo:</Typography.Text>
      {' '}
      {info.title}
    </Typography.Text>
    <Divider />
    <Typography.Text>
      <Typography.Text strong>Descripcion:</Typography.Text>
      {' '}
      {info.content ? info.content : '-'}
    </Typography.Text>
    <Divider />
    <Typography.Text>
      <Typography.Text strong>Usuario:</Typography.Text>
      {' '}
      {info.autor}
    </Typography.Text>
    <Divider />
    <Typography.Text>
      <Typography.Text strong>Provincia:</Typography.Text>
      {' '}
      {info.state}
    </Typography.Text>
    <Button type="primary" onClick={onAnswer}>Responder</Button>
    <Arrow />
  </StyledPopover>
);

// Component
const Marker = ({
  highlight,
  info,
  showInfo,
  onAnswer,
  onClose,
  user,
}) => (
  <>
    {user ? (
      <Tooltip title="Usted se encuentra aqui">
        <StyledMarker highlight={highlight} showInfo={showInfo} user />
      </Tooltip>
    ) : (
      <StyledMarker highlight={highlight} showInfo={showInfo} />
    )}
    {showInfo && (
      <Popover
        highlight={highlight}
        onAnswer={onAnswer}
        info={info}
        onClose={onClose}
      />
    )}
  </>
);

export default Marker;
