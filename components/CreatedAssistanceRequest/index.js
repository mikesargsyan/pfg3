// Vendors
import { useState, useEffect } from 'react';
import {
  List,
  Skeleton,
  Card,
  Typography,
  Divider,
  Tooltip,
  Button,
  Popconfirm,
  notification,
} from 'antd';
import styled from '@emotion/styled';
import {
  CheckCircleOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
} from '@ant-design/icons';
import moment from 'moment';
// Firebase
import { database } from '../../firebase/clientApp';
// Context
import { useUser } from '../../context/userContext';

// Styles
const StyledList = styled(List)`
  height: 100%;
  overflow-x: scroll;

  > li:last-child {
    padding-bottom: 0;
  }
`;

// Component
const CreatedAssistanceRequest = () => {
  const { user } = useUser();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const getInitialData = () => {
    const asistenciasRef = database.ref('asistencias/');

    asistenciasRef.on('value', (snapshot) => {
      setLoading(true);

      if (snapshot.val()) {
        const items = Object.entries(snapshot.val()).map((item) => ({
          ...item[1],
          id: item[0],
          showInfo: false,
          highlight: false,
        })).filter((item) => item.userId === user.uid);

        setData(items);

        if (loading) {
          setLoading(false);
        }
      } else {
        setLoading(false);
      }
    });
  };

  useEffect(() => {
    getInitialData();
  }, []);

  const getEstado = (item) => {
    if (item.closed) {
      return (
        <Tooltip title="La solicitud fue concretada correctamente">
          <Typography.Text style={{ color: 'green', fontSize: 24 }}>
            Solucionado
            {' '}
            <CheckCircleOutlined />
          </Typography.Text>
        </Tooltip>
      );
    }

    if (item.hidden) {
      return (
        <Tooltip title="La solicitud fue archivada y no es visible para nadie">
          <Typography.Text style={{ color: 'red', fontSize: 24 }}>
            Archivado
            {' '}
            <EyeInvisibleOutlined />
          </Typography.Text>
        </Tooltip>
      );
    }

    return (
      <Tooltip title="La solicitud esta visible para todos">
        <Typography.Text style={{ color: 'orange', fontSize: 24 }}>
          Visible
          {' '}
          <EyeOutlined />
        </Typography.Text>
      </Tooltip>
    );
  };

  const toggleHide = (item) => {
    const asistenciaItemRef = database.ref(`asistencias/${item.id}`);

    asistenciaItemRef.update({
      hidden: !item.hidden,
    }, () => {
      notification.success({
        message: `Solicitud ${item.hidden ? 'publicada' : 'archivada'} correctamente!`,
      });
    });
  };

  const open = (item) => {
    const asistenciaItemRef = database.ref(`asistencias/${item.id}`);

    asistenciaItemRef.update({
      closed: !item.closed,
    }, () => {
      notification.success({
        message: 'Solicitud abierta correctamente!',
      });
    });
  };

  const getFooter = (item) => {
    if (item.closed) {
      return (
        <Popconfirm
          placement="topLeft"
          title="Estas seguro que queres volver a abrir esta Solicitud? Sera visible para todos."
          okText="Si"
          onConfirm={() => open(item)}
          cancelText="No"
        >
          <Divider />
          <Button type="primary">Volver a abrir</Button>
        </Popconfirm>
      );
    }

    if (item.hidden) {
      return (
        <Popconfirm
          placement="topLeft"
          title="Estas seguro que queres volver a publicar esta Solicitud? Sera visible para todos."
          okText="Si"
          onConfirm={() => toggleHide(item)}
          cancelText="No"
        >
          <Divider />
          <Button type="primary">Volver a publicar</Button>
        </Popconfirm>
      );
    }

    if (!item.closed) {
      return (
        <Popconfirm
          placement="topLeft"
          title="Estas seguro que queres archivar esta Solicitud? No sera visible para nadie."
          okText="Si"
          onConfirm={() => toggleHide(item)}
          cancelText="No"
        >
          <Divider />
          <Button type="primary">Archivar</Button>
        </Popconfirm>
      );
    }

    return null;
  };

  if (loading) {
    return (
      <Skeleton active />
    );
  }

  return (
    <StyledList
      itemLayout="horizontal"
      dataSource={data}
      renderItem={(item) => (
        <List.Item>
          <Card type="inner" title={item.info.title} style={{ width: '100%' }}>
            <Typography.Text>
              <Typography.Text strong>Descripcion:</Typography.Text>
              {' '}
              {item.info.content ? item.info.content : '-'}
            </Typography.Text>
            <Divider />
            <Typography.Text>
              <Typography.Text strong>Fecha de creacion:</Typography.Text>
              {' '}
              {moment(item.date_created).format('DD/MM/YYYY')}
            </Typography.Text>
            <Divider />
            <Typography.Text>
              <Typography.Text strong>Estado:</Typography.Text>
              {'  '}
              {getEstado(item)}
              {getFooter(item)}
            </Typography.Text>
          </Card>
        </List.Item>
      )}
    />
  );
};

export default CreatedAssistanceRequest;
