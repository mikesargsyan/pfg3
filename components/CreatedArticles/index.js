// Vendors
import { useState, useEffect } from 'react';
import Link from 'next/link';
import {
  Form,
  Input,
  Button,
  List,
  Avatar,
  Space,
  Modal,
  Typography,
  notification,
  Skeleton,
  Divider,
  Spin,
  Select,
} from 'antd';
import { debounce } from 'lodash';
import styled from '@emotion/styled';
import Highlighter from 'react-highlight-words';
import {
  MessageOutlined,
} from '@ant-design/icons';
import moment from 'moment';
// Firebase
import { database } from '../../firebase/clientApp';
// Context
import { useUser } from '../../context/userContext';
import { useGeneralContext } from '../../context/generalContext';

// Styles
const StyledList = styled(List)`
`;

const Wrapper = styled.div`
  overflow-y: auto;
  height: 100%;
`;

const Search = styled(Input)`
  max-width: 300px;
`;

// Component
const CreatedArticles = () => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState([]);
  const [loadingList, setLoadingList] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [searchedValue, setSearchedValue] = useState('');
  const [filteredData, setFilteredData] = useState([]);
  const [searching, setSearching] = useState(false);
  const [form] = Form.useForm();
  const { user } = useUser();
  const { setGlobalLoading } = useGeneralContext();

  const getInitialData = () => {
    const foroRef = database.ref('foro/');

    foroRef.on('value', (snapshot) => {
      setLoadingList(true);

      if (snapshot.val()) {
        const items = Object.entries(snapshot.val()).map((item) => ({
          ...item[1],
          id: item[0],
        })).filter((item) => !item.closed);

        setData(items);

        if (loadingList) {
          setLoadingList(false);
        }
      } else {
        setLoadingList(false);
      }
    });
  };

  useEffect(() => {
    getInitialData();
  }, []);

  useEffect(() => {
    let aux = [];

    if (searchedValue) {
      aux = data.filter((item) => (
        item.content.toLowerCase().includes(searchedValue) ||
        item.title.toLowerCase().includes(searchedValue) ||
        item.description.toLowerCase().includes(searchedValue)));
    }

    setFilteredData(aux);
    setSearching(false);
  }, [searchedValue]);

  const onSubmit = ({ title, description }) => {
    const foroRef = database.ref('foro/');
    const newArticle = foroRef.push();
    const newArticleKey = newArticle.key;

    const createArticle = newArticle.set({
      id: newArticleKey,
      userId: user.uid,
      avatar: user.photoURL ? user.photoURL : false,
      title,
      description,
      date_created: new Date().toString(),
      content: user.displayName,
      closed: false,
    });

    createArticle
      .then(() => (
        notification.success({
          message: 'Articulo creado!',
        })
      ))
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ));
  };

  const handleModalOk = () => {
    setGlobalLoading(true);

    form
      .validateFields()
      .then((values) => {
        onSubmit(values);
        form.resetFields();
      })
      .catch((error) => {})
      .finally(() => {
        setShowModal(false);
        setGlobalLoading(false);
      });
  };

  const saveTitleChange = (string, id) => {
    const articleRef = database.ref('foro/').child(id);

    articleRef.update({
      title: string,
    });
  };

  const saveDescriptionChange = (string, id) => {
    const articleRef = database.ref(`foro/${id}`);

    articleRef.update({
      description: string,
    });
  };

  const removeArticle = (id) => {
    const articleRef = database.ref(`foro/${id}`);

    articleRef.update({
      closed: true,
    }, () => {
      notification.success({
        message: 'Articulo eliminado correctamente!',
      });
    });
  };

  const onSearch = (e) => {
    const value = e.target.value.toLowerCase();

    setSearching(true);

    (debounce(() => setSearchedValue(value), 500, { trailing: true, maxWait: 500 }))();
  };

  if (loadingList) {
    return (
      <Skeleton active />
    );
  }

  return (
    <Wrapper>
      <Modal
        key="create-article-modal"
        title="Crear un articulo"
        visible={showModal}
        onOk={handleModalOk}
        okText="Aceptar"
        cancelText="Cancelar"
        onCancel={() => setShowModal(false)}
      >
        <Form
          form={form}
          name="create-article"
          validateMessages={{ required: 'Este campo es requerido' }}
        >
          <Form.Item
            label="Titulo"
            name="title"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Input placeholder="Titulo" />
          </Form.Item>

          <Form.Item
            label="Descripcion"
            name="description"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Input.TextArea placeholder="Descripcion" />
          </Form.Item>
        </Form>
      </Modal>
      <Search placeholder="Buscar articulos" onChange={onSearch} />
      <Divider />
      <Spin tip="Buscando articulos..." spinning={loadingList}>
        <StyledList
          itemLayout="vertical"
          size="large"
          pagination={{
            pageSize: 10,
          }}
          loading={searching}
          dataSource={searchedValue ? filteredData : data}
          footer={(
            <div>
              <Button onClick={() => setShowModal(true)} type="primary" size="large">Crear un articulo</Button>
            </div>
          )}
          renderItem={(item) => (
            <List.Item
              key={item.id}
              actions={[
                <Space key="comments">
                  <Link
                    href={{
                      pathname: '/dashboard/foro/[id]',
                      query: item,
                    }}
                    as={`/dashboard/foro/${item.id}`}
                  >
                    <a>
                      <MessageOutlined />
                    </a>
                  </Link>
                  {item.comments && Object.keys(item.comments).map((x) => item.comments[x].closed).filter((bool) => !bool).length > 0 ? Object.keys(item.comments).map((x) => item.comments[x].closed).filter((bool) => !bool).length : 0}
                </Space>,
                user.uid === item.userId && <a role="button" key="edit" onClick={() => setEditMode(!editMode)}>{editMode ? 'cerrar modo edicion' : 'editar'}</a>,
                user.uid === item.userId && <a role="button" key="delte" onClick={() => removeArticle(item.id)}>eliminar articulo</a>,
              ].filter(Boolean)}
            >
              <List.Item.Meta
                avatar={item.avatar ? <Avatar src={item.avatar} /> : <Avatar>{item.content.split(' ').map((a) => a[0]).join('').toUpperCase()}</Avatar>}
                title={(
                  (editMode && user.uid === item.userId) ? (
                    <Typography.Paragraph editable={{ onChange: (v) => saveTitleChange(v, item.id) }}>{item.title}</Typography.Paragraph>
                  ) : (
                    <Link
                      href={{
                        pathname: '/dashboard/foro/[id]',
                        query: item,
                      }}
                      as={`/dashboard/foro/${item.id}`}
                    >
                      <a>
                        <Highlighter
                          searchWords={[searchedValue]}
                          autoEscape
                          textToHighlight={item.title}
                        />
                      </a>
                    </Link>
                  )
                )}
                description={(editMode && user.uid === item.userId) ? (
                  <Typography.Paragraph editable={{ onChange: (v) => saveDescriptionChange(v, item.id) }}>{item.description}</Typography.Paragraph>
                ) : (
                  <Highlighter
                    searchWords={[searchedValue]}
                    autoEscape
                    textToHighlight={item.description}
                  />
                )}
              />
              <Highlighter
                searchWords={[searchedValue]}
                autoEscape
                textToHighlight={item.content}
              />
              <br />
              {moment(item.date_created).format('MMMM Do YYYY, h:mm:ss')}
            </List.Item>
          )}
        />
      </Spin>
    </Wrapper>
  );
};

export default CreatedArticles;
