// Vendors
import { useEffect, useState } from 'react';
import styled from '@emotion/styled';
// Components
import Slides from '../Slides';
// Context
import { database } from '../../firebase/clientApp';

// Styles
const SlidesWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(250px, 1fr));
  grid-gap: 10px;
  padding: 16px;
  background: #f0f2f5;

  @media only screen and (max-width: 767px) {
    grid-template-columns: repeat(1, minmax(250px, 1fr));
  }
`;

// Component
const FooterComponent = () => {
  const [tips, setTips] = useState([]);
  const [recs, setRecs] = useState([]);
  const [news, setNews] = useState([]);

  const tipsReference = database.ref('tips/');
  const recsReference = database.ref('recs/');
  const newsReference = database.ref('news/');

  // Con este metodo llamamos a los tips
  const getTips = () => {
    tipsReference.once('value')
      .then((snapshot) => {
        setTips(Object.entries(snapshot.val()).map((item, index) => ({ id: index, ...item[1] })))
      });
  };

  const getRecs = () => {
    recsReference.once('value')
      .then((snapshot) => {
        setRecs(Object.entries(snapshot.val()).map((item, index) => ({ id: index, ...item[1] })))
      });
  };

  const getNews = () => {
    newsReference.once('value')
      .then((snapshot) => {
        setNews(Object.entries(snapshot.val()).map((item, index) => ({ id: index, ...item[1] })))
      });
  };

  useEffect(() => {
    getTips();
    getRecs();
    getNews();
  }, []);

  return (
    <SlidesWrapper>
      <Slides data={tips} />
      <Slides data={recs} />
      <Slides data={news} />
    </SlidesWrapper>
  );
};

export default FooterComponent;
