// Vendors
import {
  Typography,
  Divider,
  Form,
  Select,
  Input,
  Skeleton,
  Button,
  notification,
} from 'antd';
import {
  LoadingOutlined,
} from '@ant-design/icons';
import styled from '@emotion/styled';
// Firebase
import { database } from '../../firebase/clientApp';
// Context
import { useGeneralContext } from '../../context/generalContext';
import { useUser } from '../../context/userContext';

// Styles
const StyledForm = styled(Form)`
  max-width: 600px;
`;

const Wrapper = styled.div`
  height: 100%;
  overflow-x: scroll;
`;

// Component
const CreateAssistance = ({ loading, userPosition }) => {
  const { user } = useUser();
  const { setGlobalLoading, setGlobalLoadingTip } = useGeneralContext();

  const onSubmit = (values) => {
    setGlobalLoading(true);
    setGlobalLoadingTip('Creando solicitud...');

    const asistenciasReference = database.ref('asistencias/');
    const newAssistance = asistenciasReference.push();

    const createAssistance = newAssistance.set({
      userId: user.uid,
      info: {
        displayName: user.displayName,
        autor: user.email,
        title: values.title,
        content: values.description ? values.description : '',
        state: values.state,
      },
      coords: {
        latitude: userPosition.coords.latitude,
        longitude: userPosition.coords.longitude,
      },
      hidden: false,
      closed: false,
      date_created: new Date().toString(),
    });

    createAssistance
      .then(() => (
        notification.success({
          message: 'Solicitud creada!',
        })
      ))
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ))
      .finally(() => setGlobalLoading(false));
  };

  if (loading) {
    return (
      <Skeleton active />
    );
  }

  if (userPosition) {
    return (
      <Wrapper>
        <Typography.Title level={3}>¡Estas por crear una solicitud!</Typography.Title>
        <Typography.Title level={4}>Tene en cuenta que tu ubicacion se esta tomando con GPS pero brindaras mas informacion a la hora de cuando una persona te contacte para ayudarte.</Typography.Title>
        <Divider />
        <StyledForm
          name="create-assistance"
          layout="vertical"
          onFinish={onSubmit}
          validateMessages={{
            required: 'Este campo es requerido',
            string: {
              min: 'Tiene que tener un minimo de ${min} caracteres',
            },
          }}
        >
          <Form.Item
            label="Titulo de la solicitud"
            name="title"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Input placeholder="Necesito ayuda con las compras" />
          </Form.Item>

          <Form.Item
            label="Descripcion de la solicitud"
            name="description"
            rules={[{
              min: 20,
            }]}
            hasFeedback
          >
            <Input.TextArea placeholder="Necesito a alguien que me haga las compras hoy por la tarde" />
          </Form.Item>

          <Form.Item
            label="Tipo de solicitud"
            name="type"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Select placeholder="Secciona una opcion">
              <Select.Option value="compras">Compras</Select.Option>
              <Select.Option value="ayuda">Ayuda</Select.Option>
              <Select.Option value="salud">Salud</Select.Option>
              <Select.Option value="envio">Envio</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Provincia"
            name="state"
            rules={[{
              required: true,
            }]}
            hasFeedback
          >
            <Select placeholder="Secciona una opcion">
              <Select.Option value="Buenos Aires">Buenos Aires</Select.Option>
              <Select.Option value="Catamarca">Catamarca</Select.Option>
              <Select.Option value="Chaco">Chaco</Select.Option>
              <Select.Option value="Chubut">Chubut</Select.Option>
              <Select.Option value="Córdoba">Córdoba</Select.Option>
              <Select.Option value="Corrientes">Corrientes</Select.Option>
              <Select.Option value="Entre Ríos">Entre Ríos</Select.Option>
              <Select.Option value="Formosa">Formosa</Select.Option>
              <Select.Option value="Jujuy">Jujuy</Select.Option>
              <Select.Option value="La Pampa">La Pampa</Select.Option>
              <Select.Option value="La Rioja">La Rioja</Select.Option>
              <Select.Option value="Mendoza">Mendoza</Select.Option>
              <Select.Option value="Misiones">Misiones</Select.Option>
              <Select.Option value="Neuquén">Neuquén</Select.Option>
              <Select.Option value="Río Negro">Río Negro</Select.Option>
              <Select.Option value="Salta">Salta</Select.Option>
              <Select.Option value="San Juan">San Juan</Select.Option>
              <Select.Option value="San Luis">San Luis</Select.Option>
              <Select.Option value="Santa Cruz">Santa Cruz</Select.Option>
              <Select.Option value="Santa Fe">Santa Fe</Select.Option>
              <Select.Option value="Santiago del Estero">Santiago del Estero</Select.Option>
              <Select.Option value="Tierra del Fuego, Antártida e Isla del Atlántico Sur">Tierra del Fuego, Antártida e Isla del Atlántico Sur</Select.Option>
              <Select.Option value="Tucumán">Tucumán</Select.Option>
            </Select>
          </Form.Item>

          <Button type="primary" htmlType="submit">
            Solicitar asistencia!
          </Button>
        </StyledForm>
      </Wrapper>
    );
  }

  return (
    <Typography.Title level={3}>
      Hablita la geolocalizacion para poder crear una solicitud basada en tu ubicacion.
    </Typography.Title>
  );
};

export default CreateAssistance;
