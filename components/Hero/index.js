// Vendors
import styled from '@emotion/styled';
import { useRouter } from 'next/router';
import { Typography, Card } from 'antd';
// Context
import { useUser } from '../../context/userContext';

// Styles
const Wrapper = styled.div`
  padding: 100px 0;
  background-color: #f7f9fe;
`;

const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 0 15px;

  @media only screen and (max-width: 767px) {
    grid-template-columns: 1fr;
    padding: 0 30px;
  }
`;

const CardsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 35px;

  @media only screen and (max-width: 767px) {
    grid-template-columns: 1fr;
  }
`;

const TitleSection = styled.div`
  text-align: center;

  img {
    margin-bottom: 32px;
  }

  h3 {
    color: #1890ff;
  }

  h1 {
    color: #50658e !important;
    margin-top: 0 !important;
    font-weight: 700 !important;
  }

  span {
    margin-bottom: 50px;
    display: block;
  }
`;

const StyledCard = styled(Card)`
  max-width: 350px;
  margin: 0 auto;

  .ant-card-cover {
    border-bottom: 4px solid #1890ff;
    height: 260px;
    width: 100%;
    overflow: hidden;
  }

  h4 {
    color: #1890ff;
  }
`;

// Component
const Hero = () => {
  const { user } = useUser();
  const router = useRouter();

  return (
    <Wrapper>
      <Container>
        <TitleSection>
          <img src="/images/qarentena-logo-hero.png" alt="Qarentena Logo" />
          {/* <Typography.Title level={3}>Bienvenido a Q-arentena!</Typography.Title> */}
          <Typography.Title level={1}>Red de contacto y ayuda en tiempos de Pandemia</Typography.Title>
          <Typography.Text>Encontra todo lo que necesites en nuestras tres secciones, interactúa con otros usuarios y entre todos vamos a pasar por esta cuarentena de la mejor manera.</Typography.Text>
        </TitleSection>
        <CardsWrapper>
          <StyledCard
            hoverable
            cover={<img alt="example" src="/images/hero1.png" />}
            onClick={() => {
              if (user) {
                router.push({
                  pathname: '/dashboard/foro',
                });
              } else {
                router.push({
                  pathname: '/auth',
                  query: { redirection: '/dashboard/foro' },
                });
              }
            }}
          >
            <Typography.Title level={4}>Foro</Typography.Title>
            <Typography.Text>Comunicate con todos los usuarios mediante tus publicaciones. Comenta y likea!</Typography.Text>
          </StyledCard>
          <StyledCard
            hoverable
            cover={<img alt="example" src="/images/hero2.png" />}
            onClick={() => {
              if (user) {
                router.push({
                  pathname: '/dashboard/eventos',
                });
              } else {
                router.push({
                  pathname: '/auth',
                  query: { redirection: '/dashboard/eventos' },
                });
              }
            }}
          >
            <Typography.Title level={4}>Eventos</Typography.Title>
            <Typography.Text>Clases, talleres, ideas, cursos, shows, transmisiones en vivo …lo que que quieras compartir con los demás usuarios! Crea un evento y comparti el link a tu plataforma elegida (vivos de Instagram, canales de Youtube, reuniones de Zoom, etc) para que los demás usuarios puedan asistir.</Typography.Text>
          </StyledCard>
          <StyledCard
            hoverable
            cover={<img alt="example" src="/images/hero3.jpg" />}
            onClick={() => {
              if (user) {
                router.push({
                  pathname: '/dashboard/asistencia',
                });
              } else {
                router.push({
                  pathname: '/auth',
                  query: { redirection: '/dashboard/asistencia' },
                });
              }
            }}
          >
            <Typography.Title level={4}>Asistencia</Typography.Title>
            <Typography.Text>
              ¿No podes salir y necesitas ayuda en tu domicilio? Carga una solicitud de asistencia explicando cómo pueden ayudarte, asi otros usuarios con posibilidad de movimiento responderán a tu solicitud y se pondrán en contacto.
              <br />
              <br />
              ¿Podes salir y queres ayudar a otros que no pueden? Busca en el mapa las solicitudes de asistencia a tu alcance y respondelas para contactarte con esos usuarios.
            </Typography.Text>
          </StyledCard>
        </CardsWrapper>
      </Container>
    </Wrapper>
  );
};

export default Hero;