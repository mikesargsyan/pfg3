// Vendors

// Components
// Context
import { useUser } from '../../context/userContext';
import { useGeneralContext } from '../../context/generalContext';

// Component
const Page = ({ children }) => {
  const { user } = useUser();
  const { globalLoading } = useGeneralContext();

  return (
    user && !globalLoading ? (
      children
    ) : null
  );
};

export default Page;
