// Vendors
import {
  useState,
  useEffect,
} from 'react';
import styled from '@emotion/styled';
import {
  Skeleton,
  Typography,
  Spin,
  Divider,
  List,
  Input,
  Button,
  notification,
  Select,
  Modal,
} from 'antd';
import { debounce, clone } from 'lodash';
import {
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import 'isomorphic-fetch';
import Highlighter from 'react-highlight-words';
import moment from 'moment';
import GoogleMapReact from 'google-map-react';
// Components
import Marker from '../Marker';
// Context
import { useGeneralContext } from '../../context/generalContext';
import { database } from '../../firebase/clientApp';
import { useUser } from '../../context/userContext';

// Styles
const MapWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
`;

const SpinnerWrapper = styled.div`
  width: 100%;
  height: 100%;

  > .ant-spin-nested-loading {
    width: 100%;
    height: 100%;

    > .ant-spin-container {
      display: flex;
      width: 100%;

      @media only screen and (max-width: 991px) {
        flex-direction: column;
      }
    }
  }
`;

const StyledList = styled(List)`
  margin-right: 24px;
  max-width: 500px;
  width: 100%;
  height: 100%;
  overflow-x: scroll;
  border: none;

  @media only screen and (max-width: 991px) {
    max-width: 100%;
    margin-right: 0;
    margin-bottom: 24px;
  }

  .ant-list-items {
    /* max-height: 100vh; */
    height: 100%;
  }

  + div {
    height: 100vh !important;
    position: sticky !important;
    top: 24px;
  }

  li {
    &:first-of-type {
      border-top-width: 2px !important;
    }

    &:last-of-type {
      border-bottom-width: 2px !important;
    }
  }

  .ant-divider {
    margin: 8px 0;
  }
`;

const StyledListItem = styled(List.Item)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  transition: 0.3s all linear;
  border-top: 0.5px solid #f0f0f0 !important;
  border-left: 2px solid #f0f0f0;
  border-right: 2px solid #f0f0f0;
  border-bottom: 0.5px solid #f0f0f0 !important;
  background-color: white;
  padding: 16px;
  cursor: pointer;
`;

const Search = styled(Input)`
  max-width: 300px;
  margin-right: 16px;
`;

// Component
const SearchAssistance = ({ state, loading, userPosition }) => {
  const { user } = useUser();
  const { setGlobalLoading, setGlobalLoadingTip } = useGeneralContext();
  const [data, setData] = useState([]);
  const [loadingList, setLoadingList] = useState(true);
  const [filteredData, setFilteredData] = useState([]);
  const [stateFilter, setStateFilter] = useState(state);
  const [searchedValue, setSearchedValue] = useState('');
  const [searching, setSearching] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    title: '',
    content: '',
    autor: '',
    item: null,
  });

  useEffect(() => {
    const asistenciasRef = database.ref('asistencias/');

    asistenciasRef.on('value', (snapshot) => {
      if (snapshot.val()) {
        const items = Object.entries(snapshot.val()).map((item) => ({
          ...item[1],
          id: item[0],
          showInfo: false,
          highlight: false,
        })).filter((item) => !item.hidden && !item.closed);

        setData(items);

        if (loadingList) {
          setLoadingList(false);
        }
      } else {
        setLoadingList(false);
      }
    });
  }, []);

  useEffect(() => {
    setStateFilter(state);
  }, [state]);

  useEffect(() => {
    let aux = data;

    if (searchedValue) {
      aux = aux.filter((item) => item.info.content.toLowerCase().includes(searchedValue) || item.info.title.toLowerCase().includes(searchedValue) || item.info.autor.toLowerCase().includes(searchedValue));
    }

    if (stateFilter) {
      aux = aux.filter((item) => item.info.state === stateFilter);
    }

    setFilteredData(aux);
    setSearching(false);
  }, [data, stateFilter, searchedValue]);

  const onChildClickCallback = (element, key, show) => {
    const auxData = clone(searchedValue ? filteredData : data);
    const index = auxData.findIndex((e) => e.id === key.toString());

    if (element) {
      if (element.target.type === 'button') {
        auxData.forEach((item, i) => {
          if (i === index) {
            auxData[index].showInfo = true;
          } else {
            item.showInfo = false;
          }
        });
      } else {
        auxData[index].showInfo = show || !auxData[index].showInfo;
      }
    } else {
      auxData[index].showInfo = show || !auxData[index].showInfo;
    }

    setData(auxData);
  };

  const toggleHightlistListItem = (id) => {
    const auxData = clone(searchedValue ? filteredData : data);
    const index = auxData.findIndex((e) => e.id === id);

    auxData[index].highlight = !auxData[index].highlight;

    setData(auxData);
  };

  const onSearch = (e) => {
    const value = e.target.value.toLowerCase();

    setSearching(true);

    (debounce(() => setSearchedValue(value), 500, { trailing: true, maxWait: 500 }))();
  };

  const onFilter = (e) => setStateFilter(e);

  const onAnswer = (item) => {
    onChildClickCallback(null, item.id, true);

    setModal({
      visible: true,
      title: item.info.title,
      content: item.info.content,
      autor: item.info.autor,
      item,
      state: item.info.state,
    });
  };

  const handleModalOk = () => {
    setModal({
      ...modal,
      visible: false,
    });
    setGlobalLoading(true);
    setGlobalLoadingTip('Enviando email...');

    fetch('/api/dashboard/asistencia/answer', {
      method: 'post',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ user, item: modal.item }),
    })
      .then((res) => {
        if (res.status !== 200) {
          throw new Error();
        } else {
          const asistenciaItemRef = database.ref(`asistencias/${modal.item.id}`);

          asistenciaItemRef.set({
            closed: !modal.item.closed,
            coords: modal.item.coords,
            date_created: modal.item.date_created,
            hidden: modal.item.hidden,
            id: modal.item.id,
            info: modal.item.info,
            userId: modal.item.userId,
          }, () => {
            notification.success({
              message: 'Solicitud respondida!',
              description:
                'Se les envio un email a ambos para mas detalles',
            });
          });
        }
      })
      .catch(() => (
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema. Por favor, intentalo mas tarde!',
        })
      ))
      .finally(() => setGlobalLoading(false));
  };

  const handleModalCancel = () => {
    setModal({
      visible: false,
      title: '',
      content: '',
      autor: '',
    });
  };

  if (loading) {
    return (
      <Skeleton active />
    );
  }

  if (userPosition) {
    return (
      <>
        <Modal
          title={modal.title}
          visible={modal.visible}
          onOk={handleModalOk}
          okText="Aceptar"
          cancelText="Cancelar"
          onCancel={handleModalCancel}
        >
          <Typography.Text strong>Descripcion:</Typography.Text>
          {' '}
          {modal.content ? modal.content : '-'}
          <Divider />
          <Typography.Text strong>Usuario:</Typography.Text>
          {' '}
          {modal.autor}
          <Divider />
          <Typography.Text strong>Provincia:</Typography.Text>
          {' '}
          {modal.state}
          <Divider />
          <ExclamationCircleOutlined style={{ textAlign: 'left', display: 'block', fontSize: 24 }} />
          <br />
          <Typography.Text>Estamos apunto de enviarle un email avisandole a la persona que lo vas a asistir. Se te enviara un email a vos tambien asi mantienen una conversacion por ahi.</Typography.Text>
        </Modal>
        <Search placeholder="Buscador" onChange={onSearch} allowClear />
        <Select placeholder="Filtrar por provincia" onChange={onFilter} allowClear value={stateFilter} defaultValue="1">
          <Select.Option value="1">Selecciona una provincia</Select.Option>
          <Select.Option value="Buenos Aires">Buenos Aires</Select.Option>
          <Select.Option value="Catamarca">Catamarca</Select.Option>
          <Select.Option value="Chaco">Chaco</Select.Option>
          <Select.Option value="Chubut">Chubut</Select.Option>
          <Select.Option value="Córdoba">Córdoba</Select.Option>
          <Select.Option value="Corrientes">Corrientes</Select.Option>
          <Select.Option value="Entre Ríos">Entre Ríos</Select.Option>
          <Select.Option value="Formosa">Formosa</Select.Option>
          <Select.Option value="Jujuy">Jujuy</Select.Option>
          <Select.Option value="La Pampa">La Pampa</Select.Option>
          <Select.Option value="La Rioja">La Rioja</Select.Option>
          <Select.Option value="Mendoza">Mendoza</Select.Option>
          <Select.Option value="Misiones">Misiones</Select.Option>
          <Select.Option value="Neuquén">Neuquén</Select.Option>
          <Select.Option value="Río Negro">Río Negro</Select.Option>
          <Select.Option value="Salta">Salta</Select.Option>
          <Select.Option value="San Juan">San Juan</Select.Option>
          <Select.Option value="San Luis">San Luis</Select.Option>
          <Select.Option value="Santa Cruz">Santa Cruz</Select.Option>
          <Select.Option value="Santa Fe">Santa Fe</Select.Option>
          <Select.Option value="Santiago del Estero">Santiago del Estero</Select.Option>
          <Select.Option value="Tierra del Fuego, Antártida e Isla del Atlántico Sur">Tierra del Fuego, Antártida e Isla del Atlántico Sur</Select.Option>
          <Select.Option value="Tucumán">Tucumán</Select.Option>
        </Select>
        <Divider />
        <SpinnerWrapper>
          <Spin tip="Buscando solicitudes..." spinning={loadingList}>
            <StyledList
              dataSource={searchedValue || stateFilter ? filteredData : data}
              bordered
              loading={searching}
              renderItem={(item) => (
                <StyledListItem
                  onMouseEnter={() => toggleHightlistListItem(item.id)}
                  onMouseLeave={() => toggleHightlistListItem(item.id)}
                  onClick={(e) => onChildClickCallback(e, item.id)}
                  highlight={item.highlight}
                >
                  <Typography.Text>
                    <Typography.Text strong>Titulo:</Typography.Text>
                    {' '}
                    <Highlighter
                      searchWords={[searchedValue]}
                      autoEscape
                      textToHighlight={item.info.title}
                    />
                  </Typography.Text>
                  <Divider />
                  <Typography.Text>
                    <Typography.Text strong>Descripcion:</Typography.Text>
                    {' '}
                    <Highlighter
                      searchWords={[searchedValue]}
                      autoEscape
                      textToHighlight={item.info.content ? item.info.content : '-'}
                    />
                  </Typography.Text>
                  <Divider />
                  <Typography.Text>
                    <Typography.Text strong>Fecha de creacion:</Typography.Text>
                    {' '}
                    {moment(item.date_created).format('DD/MM/YYYY')}
                  </Typography.Text>
                  <Divider />
                  <Typography.Text>
                    <Typography.Text strong>Usuario:</Typography.Text>
                    {' '}
                    <Highlighter
                      searchWords={[searchedValue]}
                      autoEscape
                      textToHighlight={item.info.autor}
                    />
                  </Typography.Text>
                  <Divider />
                  <Typography.Text>
                    <Typography.Text strong>Provincia:</Typography.Text>
                    {' '}
                    <Highlighter
                      searchWords={[searchedValue]}
                      autoEscape
                      textToHighlight={item.info.state}
                    />
                  </Typography.Text>
                  <Divider />
                  <Button type="primary" onClick={() => onAnswer(item)}>Responder</Button>
                </StyledListItem>
              )}
            />
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyB6GDlZjjF16Hs-cBpZGMQjViBxi63xSyk' }}
              center={{
                lat: userPosition.coords.latitude,
                lng: userPosition.coords.longitude,
              }}
              onChildClick={(key) => onChildClickCallback(null, key)}
              defaultZoom={14}
            >
              {(searchedValue || stateFilter) ? (
                filteredData.map((item) => (
                  <Marker
                    key={item.id}
                    lat={item.coords.latitude}
                    lng={item.coords.longitude}
                    info={item.info}
                    showInfo={item.showInfo}
                    highlight={item.highlight}
                    onAnswer={() => onAnswer(item)}
                    onClose={() => onChildClickCallback(null, item.id)}
                  />
                ))
              ) : (
                data.map((item) => (
                  <Marker
                    key={item.id}
                    lat={item.coords.latitude}
                    lng={item.coords.longitude}
                    info={item.info}
                    showInfo={item.showInfo}
                    highlight={item.highlight}
                    onAnswer={() => onAnswer(item)}
                    onClose={() => onChildClickCallback(null, item.id)}
                  />
                ))
              )}
              <Marker
                key={99999999999}
                lat={userPosition.coords.latitude}
                lng={userPosition.coords.longitude}
                user
              />
            </GoogleMapReact>
          </Spin>
        </SpinnerWrapper>
      </>
    );
  }

  return (
    <MapWrapper>
      <Typography.Title level={3}>Hablita la geolocalizacion para mostrar las solicitudes cercanas a vos.</Typography.Title>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyB6GDlZjjF16Hs-cBpZGMQjViBxi63xSyk' }}
        yesIWantToUseGoogleMapApiInternals
        center={{
          lat: -34.603722,
          lng: -58.381592,
        }}
        defaultZoom={14}
      />
    </MapWrapper>
  );
};

export default SearchAssistance;
