// Vendors
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import styled from '@emotion/styled';
// Context
import { useGeneralContext } from '../../context/generalContext';

// Styles
const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  z-index: 999;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  background-color: #fafafa;
`;

// Component
const GlobalSpinner = () => {
  const { globalLoadingTip, globalLoading } = useGeneralContext();

  if (!globalLoading) {
    return <></>;
  }

  return (
    <Wrapper>
      <Spin
        indicator={<LoadingOutlined style={{ fontSize: 72 }} spin />}
        tip={globalLoadingTip}
        spinning={globalLoading}
      />
    </Wrapper>
  );
};

export default GlobalSpinner;
