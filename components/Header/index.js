// Vendors
import { useRouter } from 'next/router';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  LogoutOutlined
} from '@ant-design/icons';
// Context
import { useUser } from '../../context/userContext';

const { Header } = Layout;

// Component
const HeaderComponent = () => {
  const router = useRouter();
  const { user, onSignOut } = useUser();
  const isOnDashboard = router.pathname.includes('dashboard');

  const navigate = (path, as) => {
    router.push(path, as);
  };

  return (
    <Header className="header">
      {isOnDashboard ? (
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[router.pathname]}>
          <Menu.Item key="/inicio" onClick={() => navigate('/')}>
            <img src="/images/qarentena-logo.png" width="34" />
          </Menu.Item>
          <Menu.Item key="/dashboard/foro" onClick={() => navigate('/dashboard/foro')}>Foro</Menu.Item>
          <Menu.Item key="/dashboard/eventos" onClick={() => navigate('/dashboard/eventos')}>Eventos</Menu.Item>
          <Menu.Item key="/dashboard/asistencia" onClick={() => navigate('/dashboard/asistencia')}>Asistencia</Menu.Item>
          <Menu.Item onClick={onSignOut}>
            <LogoutOutlined />
            <span>Salir</span>
          </Menu.Item>
        </Menu>
      ) : (
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[router.pathname]}>
          <Menu.Item key="/inicio" onClick={() => navigate('/')}>
            Inicio
          </Menu.Item>
          {user ? (
            <Menu.Item key="/dashboard" onClick={() => navigate('/dashboard/foro')} icon={<UserOutlined />} style={{ float: 'right' }}>{user.displayName}</Menu.Item>
          ) : (
            <Menu.Item key="/auth" onClick={() => navigate('/auth')} icon={<UserOutlined />} style={{ float: 'right' }}>Iniciar sesion</Menu.Item>
          )}
        </Menu>
      )}
    </Header>
  );
};

export default HeaderComponent;
