// Vendors
import { Carousel } from 'antd';
import styled from '@emotion/styled';

// Styles
const StyledCarousel = styled(Carousel)`
  height: 100%;
  max-height: 200px;
  width: 100%;
  background: #364d79;
  overflow: hidden;
  text-align: center;

  @media only screen and (max-width: 250px) {
    grid-template-columns: 1fr;
    padding: 0 30px;
  }
`;

const ItemContent = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  h3 {
    color: white;
  }

  img {
    max-width: 250px;
    width: 100%;
    margin: auto;
  }
`;

// Component
const Slides = ({ data }) => {
  return (
    <StyledCarousel autoplay>
      {data.map((item) => (
        <ItemContent key={item.id}>
          <h3>{item.title}</h3>
          {item.link && (
            <>
              <br />
              <a href={item.link} target="_blank">Ver link presionando aca!</a>
            </>
          )}
          {item.embed_code && (
            <iframe
              title="Youtube embed"
              src={item.embed_code}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"
              allowFullscreen
            />
          )}
          {item.image && (
            <img src={item.image} title="" alt="" />
          )}
        </ItemContent>
      ))}
    </StyledCarousel>
  );
};

export default Slides;
