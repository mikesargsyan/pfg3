// Vendors
import { useState, useEffect, createContext, useContext } from 'react';

export const GeneralContext = createContext();

// Component
export const GeneralProvider = ({ children }) => {
  const [globalLoading, setGlobalLoading] = useState(true);
  const [globalLoadingTip, setGlobalLoadingTip] = useState('');

  useEffect(() => {
    if (!globalLoading) {
      setGlobalLoading('');
    }
  }, [globalLoading]);

  return (
    <GeneralContext.Provider
      value={{
        globalLoading,
        setGlobalLoading,
        globalLoadingTip,
        setGlobalLoadingTip,
      }}
    >
      {children}
    </GeneralContext.Provider>
  );
};

export const useGeneralContext = () => useContext(GeneralContext);
