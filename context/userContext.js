// Vendors
import { useRouter } from 'next/router';
import {
  useState,
  useEffect,
  createContext,
  useContext,
} from 'react';
import { notification } from 'antd';
// Firebase
import firebase from '../firebase/clientApp';
// Context
import { useGeneralContext } from './generalContext';
import { auth } from 'firebase';

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [loadingUser, setLoadingUser] = useState(false);
  const { setGlobalLoading } = useGeneralContext();
  const router = useRouter();

  const redirectToAuth = () => {
    if (!router.pathname.includes('auth')) {
      router.replace('/auth');
    }
  };

  const redirectToDashboard = () => {
    if (localStorage.getItem('redirection')) {
      router.replace(JSON.parse(localStorage.getItem('redirection')));
      localStorage.removeItem('redirection');
    } else {
      router.replace('/dashboard/foro');
    }
  };

  useEffect(() => {
    if (user && router.pathname.includes('auth')) {
      redirectToDashboard();
    } else if (!user && router.pathname.includes('dashboard')) {
      redirectToAuth();
    }
  }, [user]);

  useEffect(() => {
    // Listen authenticated user
    const unsubscriber = firebase.auth().onAuthStateChanged(async (authUser) => {
      try {
        if (authUser) {
          // User is signed in.
          const {
            uid,
            displayName,
            email,
            photoURL,
            emailVerified,
          } = authUser;
          // You could also look for the user doc in your Firestore (if you have one):
          // const userDoc = await firebase.firestore().doc(`users/${uid}`).get()
          if (authUser.emailVerified) {
            setUser({
              uid, displayName, email, photoURL, emailVerified,
            });
          } else if (JSON.parse(localStorage.getItem('facebook-login')) && authUser.emailVerified) {
            setUser({
              uid, displayName, email, photoURL, emailVerified,
            });
          }
        } else {
          setUser(null);
        }
      } catch (error) {
        // Most probably a connection error. Handle appropiately.
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un problema de conexion. Por favor, intentalo mas tarde!',
        });
      } finally {
        setLoadingUser(false);
      }
    });

    // Unsubscribe auth listener on unmount
    return () => unsubscriber();
  }, []);

  useEffect(() => {
    setGlobalLoading(loadingUser);
  }, [loadingUser]);

  const onSignOut = () => {
    setLoadingUser(true);
    setGlobalLoading(true);

    firebase.auth().signOut()
      .then(() => {
        setUser(null);
        localStorage.removeItem('facebook-login');
        localStorage.removeItem('state');
        redirectToAuth();
      })
      .catch(() => {
        notification.error({
          message: 'Ups',
          description:
            'Parece que surgio un error al querer cerrar de sesion. Por favor, intentalo mas tarde, disculpe las molestias!',
        });
      })
      .finally(() => {
        setLoadingUser(false);
        setGlobalLoading(false);
      });
  };

  const onSignIn = (values) => {
    setLoadingUser(true);

    firebase.auth().signInWithEmailAndPassword(values.email, values.password)
      .then(() => {
        redirectToDashboard();
      })
      .catch((error) => {
        notification.error({
          message: 'Ups',
          description: error.message,
        });
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  const onSignInGoogle = () => {
    setLoadingUser(true);
    const provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const token = result.credential.accessToken;
        // The signed-in user info.

        const {
          uid,
          displayName,
          email,
          photoURL,
          emailVerified,
        } = result.user;

        setUser({
          uid, displayName, email, photoURL, emailVerified,
        });

        redirectToDashboard();
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const { email } = error;
        // The firebase.auth.AuthCredential type that was used.
        const { credential } = error;
        // ...

        // notification.error({
        //   message: 'Ups',
        //   description:
        //     'Parece que surgio un iniciando sesion con Google. Por favor, intentalo mas tarde, disculpe las molestias!',
        // });
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  const onSignInFacebook = () => {
    setLoadingUser(true);
    const provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().signInWithPopup(provider)
      .then((result) => {
        // This gives you a Facebook Access Token.
        const token = result.credential.accessToken;
        // The signed-in user info.

        localStorage.setItem('facebook-login', true);

        const {
          uid,
          displayName,
          email,
          photoURL,
          emailVerified,
        } = result.user;

        setUser({
          uid, displayName, email, photoURL, emailVerified,
        });
        redirectToDashboard();
      })
      .catch((error) => {
        console.log('error', error);
        // notification.error({
        //   message: 'Ups',
        //   description:
        //     'Parece que surgio un iniciando sesion con Facebook. Por favor, intentalo mas tarde, disculpe las molestias!',
        // });
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  const deleteAccount = (values) => {
    setLoadingUser(true);

    firebase.auth().currentUser.reauthenticateWithCredential(values.email)
      .then(() => {
        // User re-authenticated.
        firebase.auth().currentUser.delete()
          .then(() => {
            redirectToAuth();
          })
          .catch(() => {
            notification.error({
              message: 'Ups',
              description:
              'Parece que surgio un error al querer eliminar la cuenta. Por favor, intentalo mas tarde, disculpe las molestias!',
            });
          })
          .finally(() => {
            setLoadingUser(false);
          });
      })
      .catch(() => {
        // An error happened.
        notification.error({
          message: 'Ups',
          description:
          'Parece que surgio un error verificando tu cuenta. Por favor, intentalo mas tarde!',
        });
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  const onSignUp = (values) => {
    setLoadingUser(true);

    firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
      .then(() => {
        firebase.auth().currentUser.updateProfile({
          displayName: `${values.name} ${values.surname}`,
        })
          .then(() => {
            firebase.auth().currentUser.sendEmailVerification()
              .then(() => {
                notification.success({
                  message: 'Email enviado!',
                  description: 'Se te envio un email de verificacion',
                });
              })
              .catch((error) => {
                notification.error({
                  message: 'Ups',
                  description: error.message,
                });
              });
          })
          .catch(() => {});
      })
      .catch((error) => {
        notification.error({
          message: 'Ups',
          description: error.message,
        }, () => redirectToAuth());
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  const onForgotAccount = (values) => {
    setLoadingUser(true);

    firebase.auth().sendPasswordResetEmail(values.email)
      .then(() => {
        notification.success({
          message: 'Enviado',
          description: 'Te hemos enviado un email para recuperar la cuenta!',
        }, () => redirectToAuth());
      })
      .catch((error) => {
        notification.error({
          message: 'Ups',
          description: error.message,
        });
      })
      .finally(() => {
        setLoadingUser(false);
      });
  };

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        loadingUser,
        setLoadingUser,
        onSignOut,
        onSignIn,
        onSignInGoogle,
        onSignInFacebook,
        onSignUp,
        onForgotAccount,
        deleteAccount,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

// Custom hook that shorhands the context!
export const useUser = () => useContext(UserContext);
