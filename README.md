This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## Getting Started

Installation:

```bash
npm install
# or
yarn
```

Starts the development server:

```bash
npm run dev
# or
yarn dev
```

Builds the application for production:
```bash
npm run build
# or
yarn build
```

Starts a production server:
```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Fetching
For remote data fetching we use [SWR](https://swr.now.sh/).

## Styles
For styles we use [styled-components](https://styled-components.com/).

## Deploys
For deploys we use [ZEIT Now Platform](https://zeit.co);

Push development server:

```bash
```

Push production:

```bash
```

## ¿Que es Next.js?

Es un framework hecho por encima de React, que nos ofrece features como [File-System Routing](https://nextjs.org/#file-system-routing), desde [Dynamic Routing](https://github.com/zeit/next.js/tree/master/examples/dynamic-routing) y mucho mas. Nos da la posiblidad de tener server side rendering si lo deseamos. Server side rendering es la tecnica de renderear todo el lado del cliente (el sitio) en el servidor y enviar todo el sitio completo totalmente rendereado al cliente. El cliente cargaria el JavaScript que recibio y mostraria normalmente el contenido. A diferencia de todo esto existe por el otro lado el metodo de client side rendering donde el cliente entra a un sitio y lo unico que tiene es un HTML vacio, y un archivo de JavaScript. En vez de traer todo el contenido del sitio desde el servidor, el contenido es generado por el browser con el JavaScript cargado en el sitio.

Para mas informacion sobre client side rendering y server side rendering [aca](https://www.freecodecamp.org/news/what-exactly-is-client-side-rendering-and-hows-it-different-from-server-side-rendering-bd5c786b340d/);

## ¿Que es React?

Es una libreria para crear interfaces de usuario. Mas informacion aca [https://reactjs.org/](https://reactjs.org/).

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/zeit/next.js/) - your feedback and contributions are welcome!