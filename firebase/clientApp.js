import firebase from 'firebase/app';
import 'firebase/auth'; // If you need iteed it
import 'firebase/database'; // If you need it

const clientCredentials = {
  apiKey: 'AIzaSyB6GDlZjjF16Hs-cBpZGMQjViBxi63xSyk',
  authDomain: 'qarentena.firebaseapp.com',
  databaseURL: 'https://qarentena.firebaseio.com',
  projectId: 'qarentena',
  storageBucket: 'qarentena.appspot.com',
  messagingSenderId: '205286925657',
  appId: '1:205286925657:web:63d8567fc6a98e5fb39848',
  measurementId: 'G-RZ06K1QFNN',
};

const init = () => {
  firebase.initializeApp(clientCredentials);
};

// Check that `window` is in scope for the analytics module!
if (typeof window !== 'undefined' && !firebase.apps.length) {
  init();
}

export const database = (() => {
  if (!firebase.apps.length) {
    init();

    return firebase.database();
  }

  return firebase.database();
})();

export default firebase;
